# Almost Autonomous Airsoft Platform (a3p)

This repository contains source code for the Almost Autonomous Airsoft Platform (a3p). It's [ROS](https://www.ros.org/) based four-wheeled mobile robot equipped with airsoft gun turret. It utilizes deep learning based image processing for object detection and tracking, which combined with distance measurement allows the robot to actively follow and shoot various moving targets.

![robot image 1](./doc/im1.png)
![robot image 2](./doc/im3.png)

## Hardware overview

 - Brain:
   - Raspberry Pi 3B+
   - Raspberry Pi Zero W
 - Muscles:
   - 4x 90 RPM DC motors
   - 1x stepper motor
   - 1x servo motor
 - Eyes: 
   - Raspberry Pi Camera HD v2 (front camera)
   - Camera HD IR-CUT OV5647 (turret camera)
   - 2x laser distance sensors
 - Digestive system:
   - LiPo 4S Battery
   - Custom [power management board](https://gitlab.com/otoshi/a3p_power_board)
 - Legs:
   - 3D-printed wheels with flexible rims
   - 3D-printed tires (Fiberflex)

## Software overview

The software is spread across several ROS nodes, each of which is responsible for a separate task. The robot is controlled by the simple GUI desktop app. The project is in deep development stage, so there is a lot of dirty code (python nodes especially). Current image processing implementation allows the robot to recognize a set of popular objects (animals, furniture, people, etc.) and track multiple object at once, however the tracking model is not really stable and loses track due to the images shake or partial oclusion. There is still a lot of work to be done in this field.

Simple GUI:

![gui](./doc/gui.png)
![gui_settings](./doc/gui_settings.png)

## 3rd party software

The a3p robot is based on ROS Melodic. It utilizes OpenCV for image processing features and [raspicam node](https://github.com/UbiquityRobotics/raspicam_node) for acquiring frames from rpi cameras. Detection and tracking mechanism is based on set of lectures from [learnopencv.com](https://www.learnopencv.com/). Qt5 is used for GUI-related stuff. Qt style is taken from Alex Huszagh's [BreezeStyleSheets](https://github.com/Alexhuszagh/BreezeStyleSheets).



