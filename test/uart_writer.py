#!/usr/bin/env python2

import serial

SERIAL_PORT = "/dev/ttyUSB0"
BAUDRATE = 115200

try:
  ser = serial.Serial(SERIAL_PORT, BAUDRATE)

  ser.bytesize = serial.EIGHTBITS

  ser.parity = serial.PARITY_NONE

  ser.stopbits = serial.STOPBITS_ONE

  if ser.isOpen():

    ser.flushInput() #flush input buffer, discarding all its contents
    ser.flushOutput()#flush output buffer, aborting current output

    maxMsgLen = 15

    while True:
          
      order = str(raw_input())

      if order == "exit":
        exit()

      orderLen = len(order)
      if orderLen > maxMsgLen:
        print("ERROR! len order > 15")
        break
      else:
        missingChars = maxMsgLen - orderLen
        order += "\r" * missingChars

      ser.write(order.encode())
      
      response = ser.readline()
      print(response)

    ser.close()

  else:
      print("cannot open serial port ")

except Exception as e:
  print ("error open serial port: " + str(e))
  exit()
