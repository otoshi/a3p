#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <stdlib.h>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/dnn.hpp>
#include "../src/imgProc/ObjectDetectorTF.h"
#include "../src/util/Types.h"

using namespace std;

objVect foundObjects;

void drawObjectsInfo(cv::Mat& frame)
{
  int i = 0;
  cv::Scalar bbColor;

  const auto frameCenter = cv::Point2i(frame.cols/2, frame.rows/2);

  for (const auto& object : foundObjects)
  {
    auto detectedObjRect = object.second;
    const char* label = object.first;
    const auto objectCenter = cv::Point2i(detectedObjRect.x + detectedObjRect.width/2, 
                                    detectedObjRect.y + detectedObjRect.height/2);

     
    bbColor = cv::Scalar(0, 255, 0); 

    cv::rectangle(frame, detectedObjRect, bbColor, 1, 4);
    cv::circle(frame, objectCenter, 5, cv::Scalar(0,0,255), -1);

    if (detectedObjRect.y < 0.1 * frame.cols)
    {
      detectedObjRect.y += 50;
    }

    cv::putText(frame, label, cv::Point(detectedObjRect.x + 20, detectedObjRect.y - 10), cv::FONT_HERSHEY_SIMPLEX, 0.7, cv::Scalar(0, 0, 255), 2, cv::LINE_AA);

  }


  return;
}



int main(int argc, char ** argv)
{
  cv::Mat frame;
  cv::VideoCapture source;
  source.open(0);
  imgProc::ObjectDetectorTF td;
  bool done = false;
  
  while (1)
  {
    source >> frame;
    if(frame.empty())
      return -1;
    if(!done)
    {
      done = true;
      cv::imwrite("test_img.jpg", frame);
    }
    foundObjects = td.detect(frame);
    drawObjectsInfo(frame);
    cv::imshow("OpenCV Tensorflow Object Detection Demo", frame);

    char c = (char)cv::waitKey(30);
    if (c == 27)
      break;
  }
  

  return 0;
}
