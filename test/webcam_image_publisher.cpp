#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

#include<opencv2/opencv.hpp>

int main (int argc, char** argv)
{
  ros::init(argc, argv, "webcam_image_publisher");
  ros::NodeHandle nh;
  image_transport::ImageTransport it(nh);
  image_transport::Publisher pub = it.advertise("raw_frames", 1);
  cv::VideoCapture camera;
  cv::Mat src;
  ROS_INFO("Started publishing from webcam to raw_frames");

  if (argc < 2)
  {
    //get frist camera
    camera.open(0);
  }
  else
  {   //use videostream supllied in arg
    camera.open(argv[1]);
  }
  cv::waitKey(20);

  sensor_msgs::ImagePtr msg;
  ros::Rate rate(32);

  while (ros::ok())
  {
    camera >> src;
    msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", src).toImageMsg();
    pub.publish(msg);
    rate.sleep();
  }
  return 0;
}
