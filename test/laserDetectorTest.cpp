#include <gtest/gtest.h>
#include <opencv2/opencv.hpp>
#include "imgProc/LaserDetectorBase.h"

#define TEST_3000_PATH PROJECT_HOME_DIR "/test/distanceMeasTestPic/3000mm.jpg"
#define TEST_2000_PATH PROJECT_HOME_DIR "/test/distanceMeasTestPic/2000mm.jpg"
#define TEST_1500_PATH PROJECT_HOME_DIR "/test/distanceMeasTestPic/1500mm.jpg"
#define TEST_1000_PATH PROJECT_HOME_DIR "/test/distanceMeasTestPic/1000mm.jpg"
#define TEST_500_PATH  PROJECT_HOME_DIR "/test/distanceMeasTestPic/500mm.jpg"

class LaserDetectorMock : imgProc::LaserDetectorBase
{
  public:

    std::pair<double, double> getDistMeas(cv::Mat& img)
    {
      return computeLaserCoords(img);
    }
};

LaserDetectorMock detector;

double pxToMm(double px)
{
  return exp((176783 + 1000*px)/36310);
}

bool pointsDetected(const char* testFilePath)
{
  cv::Mat img = cv::imread(testFilePath);
  cv::Mat scaled;
  cv::resize(img, scaled, cv::Size(640, 380));
  
  std::pair<double, double> res = detector.getDistMeas(scaled);
  
  if (res.first == -1 || res.second == -1)
  {
    cv::imshow("out", scaled);
    cv::waitKey(0);
  }

  return res.first != -1 && res.second != -1;
}

TEST (LaserDotsDetectionTest, whiteBgTest3000mm)
{
  ASSERT_TRUE(pointsDetected(TEST_3000_PATH));
}

TEST (LaserDotsDetectionTest, whiteBgTest2000mm)
{
  ASSERT_TRUE(pointsDetected(TEST_2000_PATH));
}

TEST (LaserDotsDetectionTest, whiteBgTest1500mm)
{
  ASSERT_TRUE(pointsDetected(TEST_1500_PATH));
}

TEST (LaserDotsDetectionTest, whiteBgTest1000mm)
{
  ASSERT_TRUE(pointsDetected(TEST_1000_PATH));
}

TEST (LaserDotsDetectionTest, whiteBgTest500mm)
{
  ASSERT_TRUE(pointsDetected(TEST_500_PATH));
}

int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}