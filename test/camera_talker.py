#!/usr/bin/env python2
#utils
import time
import datetime
import numpy as np
from threading import Thread
#pi camera 
from picamera import PiCamera
from picamera.array import PiRGBArray
#ROS
import rospy
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

class FPS:
    def __init__(self):
        self.m_start = None
        self.m_end = None
        self.m_frames_sum = 0

    def start(self):
        self.m_start = datetime.datetime.now()
        return self

    def stop(self):
        self.m_end = datetime.datetime.now()

    def update(self):
        self.m_frames_sum += 1

    def time(self):
        return (self.m_end - self.m_start).total_seconds()

    def getFps(self):
        return self.m_frames_sum / self.time()

    def reset(self):
        self.m_start = datetime.datetime.now()
        self.m_frames_sum = 0

# class for video streaming in own thread
class VideoThread:
    def __init__(self, resolution=(320,240), framerate = 32):
        self.camera = PiCamera()
        self.camera.resolution = resolution
        self.camera.framerate = framerate
        self.rawCapture = PiRGBArray(self.camera, size=(320, 240))
        self.stream = self.camera.capture_continuous(self.rawCapture, format="bgr", use_video_port=True)
        self.frame = None
        self.stopped = False

    def start(self):
        Thread(target=self.update, args=()).start()
        return self

    def update(self):
        #main loop reading frames from the hardware
        for f in self.stream:
            self.frame = f.array
            self.rawCapture.truncate(0)
            time.sleep(0.005)

            if self.stopped:
                self.stream.close()
                self.rawCapture.close()
                self.camera.close()
                return
    
    def read(self):
        return self.frame

    def stop(self):
        self.stopped = True


#Init ROS publisher
framerate = 32
rospy.init_node('camera_talker', anonymous=True)
pub = rospy.Publisher('raw_frames', Image, queue_size=1)
bridge = CvBridge()
rospy.set_param('framerate', framerate)

#start camera video stream and fps counter
vs = VideoThread().start()
time.sleep(2)
fps = FPS().start()
rate = rospy.Rate(framerate)

#main publisher loop
while not rospy.is_shutdown():
    image = vs.read()
    try:
        pub.publish(bridge.cv2_to_imgmsg(image, 'bgr8'))
        rate.sleep()
    except CvBridgeError as error:
        rospy.logerr(error)
    fps.update()


fps.stop()
vs.stop()
