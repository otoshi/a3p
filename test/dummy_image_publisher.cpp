#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

#include<opencv2/opencv.hpp>

int main (int argc, char** argv)
{
  ros::init(argc, argv, "dummy_image_publisher");
  ros::NodeHandle nh;
  image_transport::ImageTransport it(nh);
  image_transport::Publisher pub = it.advertise("raw_frames", 1);
  cv::Mat src;
  if (argc < 2)
  {
    //use default test img if no path supplied in the cmd argument
    src = cv::imread("/home/tg/catkin_ws/src/robot_fpv/test/test_image.jpg", cv::IMREAD_COLOR);
  }
  else
  {   //use img supllied in arg
    src = cv::imread(argv[1], cv::IMREAD_COLOR);
  }
  cv::waitKey(20);

  if (src.empty())
  {
    ROS_ERROR("Error while reading image: image is empty");
    return -1;
  }
  sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", src).toImageMsg();
  ros::Rate rate(10);

  while (ros::ok())
  {
    pub.publish(msg);
    rate.sleep();
  }
  return 0;
}