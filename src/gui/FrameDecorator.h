#pragma once

#include <opencv/opencv2/opencv.hpp>
#include "util/Types.h"

namespace gui
{

class FrameDecorator
{
private:
  const cv::Point2i frameCenter_;

public:
  FrameDecorator(const cv::Mat& frame);
  ~FrameDecorator();

  bool drawObjectsInfo(cv::Mat& frame, 
                        const objVect& objectsInfo,
                          int trackedObject);
};
  
}