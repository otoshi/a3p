#include "PowerSwitch.h"

namespace gui
{

PowerSwitch::PowerSwitch(PowerSwitchMask id, PowerSectionsState& state,
                           PowerSectionsState& cached, ros::Publisher& pub)
: QCheckBox(mapIDtoName(id)), id_(id), powerBoardStateRef_(state), 
  cachedStateRef_(cached), pub_(pub)
{
  cachedStateRef_ = powerBoardStateRef_;
  this->setFocusPolicy(Qt::FocusPolicy::ClickFocus);
  connect(this,  SIGNAL(clicked(bool)), this, SLOT(setState(bool)));
}

void PowerSwitch::setOn()
{
  cachedStateRef_ |= (uint8_t)id_;
}

void PowerSwitch::setOff()
{
  cachedStateRef_ &= ~((uint8_t)id_);
}

uint8_t PowerSwitch::getState()
{
  return powerBoardStateRef_;
}

std_msgs::String PowerSwitch::getStateMsg()
{
  std_msgs::String msg;
  msg.data = "setchn " + std::to_string(cachedStateRef_);
  ROS_INFO_STREAM("Msg to power board: " << msg.data);
  return msg;
}

QString PowerSwitch::mapIDtoName()
{
  return mapIDtoName(id_);
}

QString PowerSwitch::mapIDtoName(PowerSwitchMask id)
{
  if (id == PowerSwitchMask::SWITCH_3V3)
  {
    return QString("3V3");
  }
  else if (id == PowerSwitchMask::SWITCH_5V)
  {
    return QString("5V");
  }
  else if (id == PowerSwitchMask::SWITCH_12V)
  {
    return QString("12V");
  }
  else if (id == PowerSwitchMask::SWITCH_VBAT)
  {
    return QString("VBAT");
  }
}

void PowerSwitch::setState(bool checked)
{
  if (checked)
    setOn();
  else
    setOff();

  if (cachedStateRef_ != powerBoardStateRef_)
  {
    pub_.publish(getStateMsg());
    powerBoardStateRef_ = cachedStateRef_;
  }
}

void PowerSwitch::lock()
{
  this->setChecked(false);
  this->setDisabled(true);
}

} // namespace gui