#include "FrameDecorator.h"

#define SHOOTING_AREA_RADIUS 50


// Main idea of frameDecorator: 
// 1. Got raw image and set of data as an input. 
// 2. Decide what is to be presented on output image.
// 3. Output decorated image.
namespace gui
{

FrameDecorator::FrameDecorator(const cv::Mat& frame) : 
  frameCenter_(cv::Point2i(frame.cols/2, frame.rows/2))
{
  // TODO: frameCenter_ can be calculated only once (compile time maybe?)
}

FrameDecorator::~FrameDecorator()
{
}


bool inShootingArea(const cv::Point2i objectCenter, 
                      const cv::Point2i frameCenter_, 
                        const int radius)
{
  return (pow((objectCenter.x - frameCenter_.x),2) + 
          pow((objectCenter.y - frameCenter_.y),2) < pow(radius, 2));
}

// TODO: make tracked object a class field, change it only on purpose
bool FrameDecorator::drawObjectsInfo(cv::Mat& frame, 
                                      const objVect& objectsInfo,
                                        int trackedObject)
{
  // int i = 0;
  // cv::Scalar bbColor;
  // bool targetAcquired = false;

  // for (const auto& object : objectsInfo)
  // {
  //   auto detectedObjRect = object.second;
  //   const char* label = object.first;
  //   const auto objectCenter = cv::Point2i(detectedObjRect.x + detectedObjRect.width/2, 
  //                                   detectedObjRect.y + detectedObjRect.height/2);

  //   if (i == trackedObject && )
  //   {
  //     // check if target is in shooting area
  //     if (inShootingArea(objectCenter, frameCenter_, SHOOTING_AREA_RADIUS))
  //     {
  //       targetAcquired = true;
  //       //show tracked not acquired object in red bb
  //       bbColor = cv::Scalar(0, 0, 255);
  //     }
  //     else
  //     {
  //       targetAcquired = false;
  //       //show tracked not acquired object in orange bb
  //       bbColor = cv::Scalar(0, 100, 255);
  //     }
  //   }
  //   else
  //   {
  //     //show untracked object in green bb
  //     bbColor = cv::Scalar(0, 255, 0); 
  //   }


  //   cv::rectangle(frame, detectedObjRect, bbColor, 1, 4);
  //   cv::circle(frame, objectCenter, 5, cv::Scalar(0, 0,255), -1);

  //   // Move upper bb edge down if too close to frame edge.

  //   if (detectedObjRect.y < 0.1 * frame.cols)
  //   {
  //     detectedObjRect.y += 50;
  //   }

  //   cv::putText(frame, label, cv::Point(detectedObjRect.x + 20, 
  //                 detectedObjRect.y - 10), cv::FONT_HERSHEY_SIMPLEX, 
  //                   0.7, cv::Scalar(0, 0, 255), 2, cv::LINE_AA);
  //   i++;
  // }

  // //draw shooting area
  // cv::circle(frame, frameCenter_,
  //               SHOOTING_AREA_RADIUS, cv::Scalar(0, 255, 255), 1, CV_AA);

  //return targetAcquired;

  return false;
}

  
}