#pragma once

#include "PowerSwitch.h"

namespace gui
{

class SettingsTab : public QWidget
{

  Q_OBJECT

  public:
    explicit SettingsTab(ros::NodeHandle& node, QWidget *parent = nullptr);

  signals:
    void lowVoltage();
    void critVoltage();
    void noBattery();
    void voltageCheck(int adcMeas);

  private slots:

    void lockHighPower();
    void lockAllPower();
    void queryBatteryVoltage();

  private:

    void initCheckboxes();
    void initPowerChBoxLayout();
    void initRobotImages();
    void initRobotViewLayout();
    void initSettingsTabLayout();
    void lockPowerSwitch(QCheckBox* powerSwitch);
    void powerBoardCb(const std_msgs::String::ConstPtr& msg);
    void servoPosCb(const std_msgs::Int32::ConstPtr& msg);
    void stepperPosCb(const std_msgs::Int32::ConstPtr& msg);
    void handleAsyncMsg(const std::string& asyncMsg);
    int parseBatteryLevel(const std::string& msg);
    QImage* drawTopView(const int angle);
    QImage* drawSideView(const int angle);
    
    PowerSectionsState powerBoardState_ = 0x03;
    PowerSectionsState cachedState_ = powerBoardState_;
    QBoxLayout*   powerCheckboxLayout_ = nullptr;
    QBoxLayout*   robotViewLayout_ = nullptr;
    QBoxLayout*   settingsTabLayout_ = nullptr;
    QButtonGroup* powerSwitchBoxes_ = nullptr;
    QLabel*       topView_ = nullptr;
    QLabel*       sideView_ = nullptr;
    
    QImage* top_ = nullptr;
    QImage* topTurret_ = nullptr;
    QImage* topResult_ = nullptr;
    QPainter* topPainter_ = nullptr;

    QImage* sideBg_ = nullptr;
    QImage* sideTurret_ = nullptr;
    QImage* sideBase_ = nullptr;
    QImage* sideResult_ = nullptr;
    QPainter* sidePainter_ = nullptr;

    PowerSwitch powerSwitch3V3;
    PowerSwitch powerSwitch5V;
    PowerSwitch powerSwitch12V;
    PowerSwitch powerSwitchVBat;

    ros::NodeHandle& nh_;
    ros::Publisher  powerBoardPublisher_;
    ros::Subscriber powerBoardSubscriber_;
    ros::Subscriber servoPosSubscriber_;
    ros::Subscriber stepperPosSubscriber_;
};

} // namespace gui