#pragma once

#include <ros/ros.h>
#include "std_msgs/String.h"
#include <QWidget>
#include <QKeyEvent>
#include <QApplication>
#include <QPushButton>
#include <QLabel>
#include <QLayout>

namespace gui
{

class KeyPress : public QWidget
{
  Q_OBJECT

public:
  KeyPress(ros::Publisher& publisher, QWidget *parent = 0);
 
protected:
  void keyPressEvent(QKeyEvent *) override;
  void keyReleaseEvent(QKeyEvent *) override;
 
private:
  QLabel *myLabel;
  QVBoxLayout *mainLayout;
  ros::Publisher& publisher_;

  void publishKeyEvent(int key, bool pressed);
  void notifyKeyEvent(int key, bool pressed);
};

} // namespace gui