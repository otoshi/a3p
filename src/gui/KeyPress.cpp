#include "KeyPress.h"

namespace gui
{

KeyPress::KeyPress(ros::Publisher& publisher, QWidget *parent) :
    QWidget(parent), publisher_(publisher)
{
    myLabel = new QLabel("LABEL");
    mainLayout = new QVBoxLayout;
    mainLayout->addWidget(myLabel);
    setLayout(mainLayout);
}

void KeyPress::publishKeyEvent(int key, bool pressed)
{
  std_msgs::String msg;
  std::stringstream ss; 

  if (pressed)
  {
    ss << "+";
  }
  else
  {
    ss << "-";
  }
  
  ss << (char)key;
  msg.data = ss.str();
  publisher_.publish(msg);

    ROS_INFO("Sending button event: [%s]", msg.data.c_str());

  return;
}

void KeyPress::notifyKeyEvent(int key, bool pressed)
{
  QString text;
  std_msgs::String msg;
  if (pressed)
  {
    text = "You pressed ";
  }
  else
  {
    text = "You released ";
  }
  
  text.append((char)key);

  myLabel->setText(text);

  return;
}
 
void KeyPress::keyPressEvent(QKeyEvent *event)
{
  if (event->isAutoRepeat() == false)
  {
    notifyKeyEvent(event->key(), true);
    publishKeyEvent(event->key(), true);
  }

}
 
void KeyPress::keyReleaseEvent(QKeyEvent *event)
{
  if (event->isAutoRepeat() == false)
  {
    notifyKeyEvent(event->key(), false);
    publishKeyEvent(event->key(), false);
  }
}

} // namespace gui