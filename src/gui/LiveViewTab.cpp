#include "LiveViewTab.h"
#include "util/SpeedTest.h"
#include "a3p/DistanceMeas.h"

#include <qdesktopwidget.h>

constexpr int SHOOTING_AREA_RADIUS = 20;

constexpr int DEFAULT_FRAMERATE = 30;

namespace gui
{

LiveViewTab::LiveViewTab(ros::NodeHandle& node, QWidget *parent)
: QWidget(parent), nh(node), turretCameraGrabber_("/raspicam_node_front/image")
{
  screenRect_ = QApplication::desktop()->screenGeometry();

  initFrontCameraLabel();
  initTurretCameraLabel();
  initPrintKeyboardInputLabel();
  initFpsLabel();
  initCheckboxes();
  initShotButton();
  initCameraWindowsLayout();
  initLiveTabLayout();
  setLayout(liveViewTabLayout_);
  initTimers(DEFAULT_FRAMERATE);

  QTimer::singleShot(0, frontCameraLabel_, SLOT(setFocus()));

  targetCoordPublisher_ = nh.advertise<std_msgs::String>("coords/target", 1);

  distMeasClient_ = nh.serviceClient<a3p::DistanceMeas>("distance_meas");

  imgProcHandler_.setTrackedObject(&selectedObjectToTrack_);

  connect(shotButton_, SIGNAL(clicked()), parent, SLOT(shotEvent()));
}

void LiveViewTab::initLiveTabLayout()
{
  liveViewTabLayout_ = new QBoxLayout(QBoxLayout::TopToBottom);
  liveViewTabLayout_->addLayout(cameraWindowsLayout_);
  liveViewTabLayout_->addWidget(fpsLabel_);
  liveViewTabLayout_->addWidget(printKeyboardInputLabel_);
  liveViewTabLayout_->addWidget(rawImgChBox_);
  liveViewTabLayout_->addWidget(procImgChBox_);
  liveViewTabLayout_->addWidget(noneImgChBox_);
  liveViewTabLayout_->addWidget(shotButton_);

  return;
}

void LiveViewTab::initCameraWindowsLayout()
{
  cameraWindowsLayout_ = new QBoxLayout(QBoxLayout::LeftToRight);
  cameraWindowsLayout_->addWidget(frontCameraLabel_, 0, Qt::AlignBottom | Qt::AlignCenter);
  cameraWindowsLayout_->addWidget(turretCameraLabel_, 0, Qt::AlignBottom | Qt::AlignCenter);
}

void LiveViewTab::initFrontCameraLabel()
{
  frontCameraLabel_ = new CameraWindow(QSize
                        (screenRect_.width()/3, screenRect_.height()/3), "Front camera");

  frontCameraLabel_->acceptWindowHandler(&cameraFocusHandler_);

  return;
}

void LiveViewTab::initTurretCameraLabel()
{
  turretCameraLabel_ = new CameraWindow(QSize
                        (screenRect_.width()/3, screenRect_.height()/3), "Turret camera");

  turretCameraLabel_->acceptWindowHandler(&cameraFocusHandler_);

  return;
}

void LiveViewTab::initCheckboxes()
{
  previewModeBoxes_ = new QButtonGroup();

  rawImgChBox_  = new QCheckBox("Live preview");
  procImgChBox_ = new QCheckBox("Object detection");
  noneImgChBox_ = new QCheckBox("No preview");

  previewModeBoxes_->addButton(rawImgChBox_);
  previewModeBoxes_->addButton(procImgChBox_);
  previewModeBoxes_->addButton(noneImgChBox_);

  previewModeBoxes_->setExclusive(true);

  rawImgChBox_ -> setChecked(true);

  rawImgChBox_ ->setFocusPolicy(Qt::FocusPolicy::ClickFocus);
  procImgChBox_->setFocusPolicy(Qt::FocusPolicy::ClickFocus);
  noneImgChBox_->setFocusPolicy(Qt::FocusPolicy::ClickFocus);

  connect(rawImgChBox_ , SIGNAL(clicked(bool)), this, SLOT(rawView(bool)));
  connect(procImgChBox_, SIGNAL(clicked(bool)), this, SLOT(procView(bool)));
  connect(noneImgChBox_, SIGNAL(clicked(bool)), this, SLOT(noneView(bool)));
  
  return;
}

void LiveViewTab::initShotButton()
{
  shotButton_ = new QPushButton("SHOOT!");

  shotButton_->setFocusPolicy(Qt::FocusPolicy::ClickFocus);
}

void LiveViewTab::initPrintKeyboardInputLabel()
{
  printKeyboardInputLabel_ = new QLabel("Distance measurement");
  printKeyboardInputLabel_->setFrameStyle(QFrame::Panel | QFrame::Plain);
  printKeyboardInputLabel_->setLineWidth(1);

  printKeyboardInputLabel_->setFixedSize(QSize(300,80));

  return;
}

void LiveViewTab::initFpsLabel()
{
  fpsLabel_ = new QLabel("0");
  fpsLabel_->setFrameStyle(QFrame::Panel | QFrame::Plain);
  fpsLabel_->setLineWidth(1);
  fpsLabel_->setFixedWidth(160);
}

void LiveViewTab::initTimers(int framerate)
{
  frameTimer_ = new QTimer(this);
  connect(frameTimer_, SIGNAL(timeout()), this, SLOT(updateFrontCamera()));
  connect(frameTimer_, SIGNAL(timeout()), this, SLOT(updateTurretCamera()));
  frameTimer_->start(1000/framerate); // ms

  videoInterruptCheckTimer_ = new QTimer(this);
  connect(videoInterruptCheckTimer_, SIGNAL(timeout()), this, SLOT(streamInterrupted()));
  videoInterruptCheckTimer_->start(1000); // ms

  distMeasTimer_ = new QTimer(this);
  connect(distMeasTimer_, SIGNAL(timeout()), this, SLOT(measureDistanceToTarget()));
  distMeasTimer_->start(100);

  return;
}

//
// TODO: Refactoring: Make updateCamera methods 
//       members of camera widget class.
//

void LiveViewTab::updateTurretCamera()
{
  cv::Mat frame;

  frame = turretCameraGrabber_.getImg();
  
  if (frame.empty())
  {
    ROS_WARN_THROTTLE(1, "Frame empty");
    return;
  }

  cv::cvtColor(frame, frame, cv::COLOR_RGB2RGBA);
  
  //conversion from Mat to QImage
  QImage qimage = QImage((uchar*) frame.data, frame.cols, 
            frame.rows, frame.step, QImage::Format_ARGB32);

  turretCameraLabel_->setPixmap(QPixmap::fromImage(qimage));
}


void LiveViewTab::updateFrontCamera()
{
  cv::Mat frame;

  if (interrupted_)
  {
    return;
  }

  frame = imgProcHandler_.getRawImg();
  
  if (frame.empty())
  {
    ROS_WARN_THROTTLE(1, "Frame empty");
    return;
  }

  //
  // TODO: Change to blocking mechanism.
  //

  if (prevFrameIndex_ == imgProcHandler_.getFrameIndex())
  {
    return;
  }

  if (previewMode_ == previewMode::PROCESSED)
  {
    detectedObjectsInfo_ = imgProcHandler_.detectObjects(frame);
  }

  // Prevent drawn info from being detected as objects

  cv::Mat outFrame = std::move(frame);

  if (previewMode_ == previewMode::PROCESSED)
  {
    drawObjectsInfo(outFrame);
  }

  #ifdef USE_NIGHTVISION
  cv::cvtColor(outFrame, outFrame, cv::COLOR_RGB2GRAY);
  QImage::Format fmt = QImage::Format_Grayscale8;
  #else
  cv::cvtColor(outFrame, outFrame, cv::COLOR_RGB2RGBA);
  QImage::Format fmt = QImage::Format_ARGB32;
  #endif

  //conversion from Mat to QImage
  QImage qimage = QImage((uchar*) outFrame.data, outFrame.cols, 
            outFrame.rows, outFrame.step, fmt);

  frontCameraLabel_->setPixmap(QPixmap::fromImage(qimage).scaled(frontCameraLabel_->size(), Qt::KeepAspectRatio));

  currentFrameIndex_++;

  prevFrameIndex_ = imgProcHandler_.getFrameIndex();
}

inline bool inShootingArea(const cv::Point2i objectCenter, const cv::Point2i frameCenter, const int radius)
{
  return (pow((objectCenter.x - frameCenter.x),2) + 
          pow((objectCenter.y - frameCenter.y),2) < pow(radius, 2));
}

//TODO: rename this methods it is doing more things than just drawing
// Move to separate class (Frame decorator)

void LiveViewTab::drawObjectsInfo(cv::Mat& frame)
{
  int i = 0;
  cv::Scalar bbColor;

  const auto frameCenter = cv::Point2i(frame.cols/2, frame.rows/2);

  for (const auto& object : detectedObjectsInfo_)
  {
    auto detectedObjRect = object.second;
    const char* label = object.first;
    const auto objectCenter = cv::Point2i(detectedObjRect.x + detectedObjRect.width/2, 
                                    detectedObjRect.y + detectedObjRect.height/2);

    if (i == selectedObjectToTrack_)
    {
      //FIXME: create custom msg fo this
      std::string coordsStr =   std::to_string(objectCenter.x) + " " 
                              + std::to_string(objectCenter.y) + " "
                              + std::to_string(frameCenter.x) + " "
                              + std::to_string(frameCenter.y);
      std_msgs::String coordsMsg;
      coordsMsg.data = coordsStr;
      targetCoordPublisher_.publish(coordsMsg);

      // check if target is in shooting area
      if (inShootingArea(objectCenter, frameCenter, SHOOTING_AREA_RADIUS))
      {
        targetAcquired_ = true;
        //show tracked not acquired object in red bb
        bbColor = cv::Scalar(0, 0, 255);
      }
      else
      {
        targetAcquired_ = false;
        //show tracked not acquired object in orange bb
        bbColor = cv::Scalar(0, 100, 255);
      }
    }
    else
    {
      //show untracked object in green bb
      
      bbColor = cv::Scalar(0, 255, 0); 
    }

    if (selectedObjectToTrack_ == -1)
    {
      targetAcquired_ = false;
    }

    cv::rectangle(frame, detectedObjRect, bbColor, 1, 4);
    cv::circle(frame, objectCenter, 5, cv::Scalar(0,0,255), -1);

    if (detectedObjRect.y < 0.1 * frame.cols)
    {
      detectedObjRect.y += 50;
    }

    cv::putText(frame, label, cv::Point(detectedObjRect.x + 20, detectedObjRect.y - 10), cv::FONT_HERSHEY_SIMPLEX, 0.7, cv::Scalar(0, 0, 255), 2, cv::LINE_AA);
    i++;
  }

  //draw shooting area
  cv::circle(frame, frameCenter,
                SHOOTING_AREA_RADIUS/4, cv::Scalar(0, 255, 255), 1, CV_AA);

  shotButton_->setEnabled(targetAcquired_);

  return;
}

void LiveViewTab::selectObjectToTrack()
{
  ROS_INFO("Selected next target.");

  selectedObjectToTrack_++;
  if (detectedObjectsInfo_.size() == 0)
  {
    selectedObjectToTrack_ = -1;
  }
  else
  {
    selectedObjectToTrack_ %= detectedObjectsInfo_.size();
  }

  ROS_DEBUG_STREAM("Selected object has now index: " << selectedObjectToTrack_);
}

void LiveViewTab::resetObjectToTrack()
{
  ROS_INFO("Target tracking stopped");

  selectedObjectToTrack_ = -1;
}

void LiveViewTab::streamInterrupted()
{
  if (lastFrameIndex_ == imgProcHandler_.getFrameIndex())
  {
    ROS_WARN_THROTTLE(2, "Stream interrupted.");
    frontCameraLabel_->clear();
    frontCameraLabel_->setText("Video not available.");
    frontCameraLabel_->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    interrupted_ = true;
  }
  else
  {
    interrupted_ = false;
  }
  
  lastFrameIndex_ = imgProcHandler_.getFrameIndex();

  //meas fps
  ROS_DEBUG_STREAM("FPS: " << currentFrameIndex_);
  QString fpsText;
  fpsText.append("FPS: ");
  fpsText.append(std::to_string(currentFrameIndex_).c_str());
  fpsLabel_->setText(fpsText);
  currentFrameIndex_ = 0;
}

void LiveViewTab::rawView(bool checked)
{
  ROS_INFO("raw view called");  
  
  if (previewMode_ == previewMode::NONE)
  {
    frameTimer_->start();
    videoInterruptCheckTimer_->start();
  }
    
  previewMode_ = previewMode::RAW;
}

void LiveViewTab::procView(bool checked)
{
  ROS_INFO("proc view called");
  
  if (previewMode_ == previewMode::NONE)
  {
    frameTimer_->start();
    videoInterruptCheckTimer_->start();
  }

  previewMode_ = previewMode::PROCESSED;
}

void LiveViewTab::noneView(bool checked)
{
  ROS_INFO("none view called");
  previewMode_ = previewMode::NONE;
  frameTimer_->stop();
  videoInterruptCheckTimer_->stop();
  frontCameraLabel_->clear();
  frontCameraLabel_->setText("Preview off");
  turretCameraLabel_->clear();
  turretCameraLabel_->setText("Preview off");
}

void LiveViewTab::resizeEvent(QResizeEvent* event)
{
  double scaleFactorW = (double)event->size().width() / (double)event->oldSize().width();
  double scaleFactorH = (double)event->size().height() / (double)event->oldSize().height();

  if (scaleFactorH <= 0 || scaleFactorH >= 2 ||
      scaleFactorW <= 0 || scaleFactorW >= 2 || 
      event->size().height() >= screenRect_.height() ||
      event->size().width() >= screenRect_.width())
  {
    scaleFactorH = 1;
    scaleFactorW = 1;
  }

  //turretCameraLabel_->resize(scaleFactorW);
}

void LiveViewTab::measureDistanceToTarget()
{
  a3p::DistanceMeas srv;
  if (distMeasClient_.call(srv))
  {
    // targetDistanceInfo_.distPx = srv.response.distPx;
    // targetDistanceInfo_.angleDeg = srv.response.angleDeg;

    pixDists_.push(srv.response.distPx);

    static int counter = 0;

    if (counter++ % 5 == 0)
    {
      printDistanceToTarget();
      counter = 0;
    }  
  }
  else
  {
    ROS_ERROR("Failed to call service dist_meas.");
  }
}

void LiveViewTab::printDistanceToTarget()
{
  int sum = 0;
  int denom = pixDists_.size();
  bool resultInvalid = false;

  while (!pixDists_.empty())
  {
    const int& currMeas = pixDists_.front();
    pixDists_.pop();

    if (currMeas == -1)
    {
      resultInvalid = true;
    }

    sum += currMeas;
  }

  if (resultInvalid)
  {
    ROS_DEBUG_STREAM("Meas distance: error");
    printKeyboardInputLabel_->setText("Distance: -");
    return;
  }
  
  double distMm = pxToMm(sum/denom);

  ROS_DEBUG_STREAM("Meas distance: " << (int)distMm << " m");

  QString text("Distance: "); 
  text.append(QString::number(distMm/1000, 'f', 2));
  text.append(" m");
  printKeyboardInputLabel_->setText(text);
  return;
}

double LiveViewTab::pxToMm(double px)
{
  return exp((176783 + 1000*px)/36310);
}

} // namespace gui