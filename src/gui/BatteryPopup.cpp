#include "BatteryPopup.h"

#include <QTimer>
#include <QLabel>
#include <QFont>
#include <QPainter>
#include <QPaintEvent>
#include <QCoreApplication>
#include <QPropertyAnimation>

#include <QDebug>

namespace gui
{

const QColor DEFAULT_BG_COLOR(0, 150, 150);
const QColor TEXT_COLOR(0, 0, 0);

const QFont FONT("Segoe UI", 16);
const int DISPLAY_TIME = 1500;

NotifyMessageBox::NotifyMessageBox(const QString& text, QWidget* parent)
    : NotifyMessageBox(text, DEFAULT_BG_COLOR, DISPLAY_TIME, parent)
{}

NotifyMessageBox::NotifyMessageBox(const QString& text, const QColor& bgColor, int milliseconds, QWidget* parent)
    : QWidget(parent)
    , label_(text)
    , bgColor_(bgColor)
    , opacity_(1.0)
    , showTimeMs_(milliseconds)
{
  setFont(FONT);

  label_.prepare(QTransform(), FONT);
  setWindowFlags(windowFlags() | Qt::FramelessWindowHint | Qt::WindowTitleHint);

  if (parent)
  {
    QPoint offset(label_.size().width() / 2 + 40, label_.size().height() / 2 + 40);
    setGeometry(QRect(parent->rect().center() - offset, parent->rect().center() + offset));
  }
  else
  {
    resize(270, 80);
  }
}

NotifyMessageBox::~NotifyMessageBox()
{}

void NotifyMessageBox::showImmediatly()
{
    show();
    QCoreApplication::processEvents();
}

void NotifyMessageBox::run()
{
    show();
    update();
    QTimer::singleShot(showTimeMs_, this, &NotifyMessageBox::fadeOut);
}

void NotifyMessageBox::fadeOut()
{
    QPropertyAnimation* animation = new QPropertyAnimation(this, "opacity", this);
    connect(animation, &QPropertyAnimation::finished, this, &NotifyMessageBox::deleteLater);

    animation->setDuration(500);
    animation->setStartValue(1.);
    animation->setEndValue(0.);
    animation->start(QAbstractAnimation::DeleteWhenStopped);
}

void NotifyMessageBox::setOpacity(qreal opacity)
{
    opacity_ = opacity;
    update();
}

qreal NotifyMessageBox::opacity() const
{
    return opacity_;
}

void NotifyMessageBox::showMessage(const QString& message, const QColor& bgColor, int milliseconds, QWidget* parent)
{
    (new NotifyMessageBox(message, bgColor, milliseconds, parent))->run();
}

void NotifyMessageBox::showMessage(const QString& message, QWidget* parent)
{
    showMessage(message, DEFAULT_BG_COLOR, parent);
}

void NotifyMessageBox::showMessage(const QString& message, const QColor& bgColor, QWidget* parent)
{
    showMessage(message, bgColor, DISPLAY_TIME, parent);
}

void NotifyMessageBox::paintEvent(QPaintEvent* event)
{
    QPainter p(this);

    p.setOpacity(opacity_);
    p.fillRect(event->rect(), bgColor_);
    p.setPen(TEXT_COLOR);
    p.drawRect(event->rect().adjusted(0, 0, -1, -1));
    p.setFont(font());

    QSize halfSize = label_.size().toSize() / 2;
    p.drawStaticText(rect().center() -= QPoint(halfSize.width(), halfSize.height()), label_);
}

}
