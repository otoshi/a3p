#include "CameraWindow.h"

namespace gui
{

CameraWindow::CameraWindow(const QSize size, const QString text, QWidget* parent) 
: QLabel(parent), initSize_(size)
{
  this->setBackgroundRole(QPalette::Base);
  this->setFocusPolicy(Qt::StrongFocus);
  this->setAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
  this->setScaledContents(true);
  this->setText(text);
  this->setFrameStyle(QFrame::Panel | QFrame::Plain);
  this->setLineWidth(1);
  this->setFixedSize(initSize_/2);
  //this->setMinimumSize(initSize_/2);
  //this->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
}

void CameraWindow::focusInEvent(QFocusEvent *event)
{
  for (auto& cameraWindow : (*cameraWindows_))
  {
    if (cameraWindow == this)
    {
      this->setFixedSize(initSize_);
    }
    else
    {
      cameraWindow->setFixedSize(initSize_/2);
    }
  }
}

void CameraWindow::acceptWindowHandler(CameraFocusHandler* cfh)
{
  cameraWindows_ = &(cfh->cameraWindows);
  cameraWindows_->push_back(this);
}

void CameraWindow::resize(double scaleFactor)
{
  // for (auto& cameraWindow : (*cameraWindows_))
  // {
  //   initSize_ *= scaleFactor;
  //   cameraWindow->setFixedSize(cameraWindow->size() * scaleFactor); 
  // }
}

} // namespace gui