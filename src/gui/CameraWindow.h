#pragma once

#include <QWidget>
#include <QLabel>
#include <QKeyEvent>

namespace gui
{

class CameraFocusHandler;

class CameraWindow : public QLabel
{
  public:
  CameraWindow(const QSize size = (QSize(640,420)), QString text = "default", QWidget* parent = nullptr);
  virtual void acceptWindowHandler(CameraFocusHandler *cfh);
  virtual void resize(double scaleFactor);

  protected:
  virtual void focusInEvent(QFocusEvent *event);

  std::vector<CameraWindow *> *cameraWindows_;

  private:
  QSize initSize_;
};

class CameraFocusHandler
{
  friend class CameraWindow;

  private:
  std::vector<CameraWindow *> cameraWindows;
};

} // namespace gui