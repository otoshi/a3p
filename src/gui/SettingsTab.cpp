#include "SettingsTab.h"

#include <QPainter>

#define IMAGE_TOP_PATH PROJECT_HOME_DIR "/doc/top_3.png"
#define IMAGE_TURRET_TOP_PATH PROJECT_HOME_DIR "/doc/turret_top.png"
#define IMAGE_SIDE_BG_PATH PROJECT_HOME_DIR "/doc/side_0.png"
#define IMAGE_SIDE_TURRET_PATH PROJECT_HOME_DIR "/doc/side_1.png"
#define IMAGE_SIDE_BASE_PATH PROJECT_HOME_DIR "/doc/side_2.png"
namespace gui
{

constexpr std::string_view LOW_VOLTAGE_MESSAGE   = "ASYNC WARN! BATTERY LOW! Switching off vBat and 12V sections.\n";
constexpr std::string_view CRIT_VOLTAGE_MESSAGE  = "ASYNC WARN! BATTERY CRITICALLY LOW! All power sections off!\n";
constexpr std::string_view BAT_NOT_FOUND_MESSAGE = "ASYNC WARN! NO BATTERY DETECTED!\n";

SettingsTab::SettingsTab(ros::NodeHandle& node, QWidget *parent)
: nh_(node),
  powerSwitch3V3(PowerSwitchMask::SWITCH_3V3, powerBoardState_, cachedState_, powerBoardPublisher_),
  powerSwitch5V(PowerSwitchMask::SWITCH_5V, powerBoardState_, cachedState_, powerBoardPublisher_),
  powerSwitch12V(PowerSwitchMask::SWITCH_12V, powerBoardState_, cachedState_, powerBoardPublisher_),
  powerSwitchVBat(PowerSwitchMask::SWITCH_VBAT, powerBoardState_, cachedState_, powerBoardPublisher_)
{
  initCheckboxes();
  initPowerChBoxLayout();
  initRobotImages();
  initRobotViewLayout();
  initSettingsTabLayout();
  setLayout(settingsTabLayout_);

  connect(this, SIGNAL(lowVoltage()), parent, SLOT(showLowVoltageWarn()));
  connect(this, SIGNAL(critVoltage()), parent, SLOT(showCritVoltageWarn()));
  connect(this, SIGNAL(noBattery()), parent, SLOT(showMissingBatWarn()));
  connect(this, SIGNAL(voltageCheck(int)), parent, SLOT(updateBatteryLevel(int)));

  QTimer* videoInterruptCheckTimer_ = new QTimer(this);

  connect(videoInterruptCheckTimer_, SIGNAL(timeout()), this, SLOT(queryBatteryVoltage()));

  videoInterruptCheckTimer_->start(5000); // ms
}

void SettingsTab::initPowerChBoxLayout()
{
  powerCheckboxLayout_ = new QBoxLayout(QBoxLayout::LeftToRight);
  powerCheckboxLayout_->addWidget(&powerSwitch3V3,  0, Qt::AlignBottom | Qt::AlignLeft);
  powerCheckboxLayout_->addWidget(&powerSwitch5V,   0, Qt::AlignBottom | Qt::AlignLeft);
  powerCheckboxLayout_->addWidget(&powerSwitch12V,  0, Qt::AlignBottom | Qt::AlignLeft);
  powerCheckboxLayout_->addWidget(&powerSwitchVBat, 0, Qt::AlignBottom | Qt::AlignLeft);
  
  return;
}

void SettingsTab::initRobotImages()
{
  //FIXME: fix memory leaks

  topView_ = new QLabel();
  sideView_ = new QLabel();
  
  topView_->setFrameStyle(QFrame::NoFrame);

  top_ = new QImage(IMAGE_TOP_PATH);
  topTurret_ = new QImage(IMAGE_TURRET_TOP_PATH);
  topResult_ = new QImage(top_->width(), top_->height(), QImage::Format_ARGB32);
  topPainter_ = new QPainter(topResult_);

  sideBg_ = new QImage(IMAGE_SIDE_BG_PATH);
  sideTurret_ = new QImage(IMAGE_SIDE_TURRET_PATH);
  sideBase_ = new QImage(IMAGE_SIDE_BASE_PATH);
  sideResult_ = new QImage(sideBg_->width(), sideBg_->height(), QImage::Format_ARGB32);
  sidePainter_ = new QPainter(sideResult_);
  sidePainter_->setRenderHint(QPainter::Antialiasing, true);
}

void SettingsTab::initRobotViewLayout()
{
  robotViewLayout_ = new QBoxLayout(QBoxLayout::LeftToRight);

  topView_->setPixmap(QPixmap::fromImage(*drawTopView(0)));
  sideView_->setPixmap(QPixmap::fromImage(*drawSideView(0)));
  topView_->setScaledContents(true);
  sideView_->setScaledContents(true);

  robotViewLayout_->addWidget(topView_);
  robotViewLayout_->addWidget(sideView_);
}

QImage* SettingsTab::drawSideView(const int angle)
{
  sidePainter_->save();
  sidePainter_->drawImage(0, 0, *sideBg_);

  QPoint rotationCenter((sideBg_->width() - sideTurret_->width())/2, (sideBg_->height() - sideTurret_->height())/2);
  QPoint p2(sideBg_->width()/2 + 110, sideBg_->height()/2 - 75);

  sidePainter_->translate(p2);
  sidePainter_->rotate(angle);
  sidePainter_->translate(-p2);
  sidePainter_->drawImage(rotationCenter, *sideTurret_);
  sidePainter_->restore();
  sidePainter_->drawImage(0, 0, *sideBase_);

  return sideResult_;
}

QImage* SettingsTab::drawTopView(const int angle)
{
  topPainter_->save();
  topPainter_->drawImage(0, 0, *top_);

  QPoint rotationCenter((top_->width() - topTurret_->width())/2, (top_->height() - topTurret_->height())/2);
  QPoint p2(top_->width()/2, top_->height()/2);

  topPainter_->translate(p2);
  topPainter_->rotate(angle);
  topPainter_->translate(-p2);
  topPainter_->drawImage(rotationCenter, *topTurret_);
  topPainter_->restore();

  return topResult_;
}

void SettingsTab::initSettingsTabLayout()
{
  settingsTabLayout_ = new QBoxLayout(QBoxLayout::TopToBottom);

  settingsTabLayout_->addLayout(robotViewLayout_);
  settingsTabLayout_->addLayout(powerCheckboxLayout_);
}

void SettingsTab::initCheckboxes()
{
  powerBoardPublisher_ = nh_.advertise<std_msgs::String>("power_board/orders", 10);
  powerBoardSubscriber_ = nh_.subscribe("power_board/responses", 10, &SettingsTab::powerBoardCb, this);
  servoPosSubscriber_ = nh_.subscribe("motor_positions/servo", 1, &SettingsTab::servoPosCb, this);
  stepperPosSubscriber_ = nh_.subscribe("motor_positions/stepper", 10, &SettingsTab::stepperPosCb, this);

  powerSwitchBoxes_ = new QButtonGroup();

  powerSwitchBoxes_->addButton(&powerSwitch3V3);
  powerSwitchBoxes_->addButton(&powerSwitch5V);
  powerSwitchBoxes_->addButton(&powerSwitch12V);
  powerSwitchBoxes_->addButton(&powerSwitchVBat);

  powerSwitchBoxes_->setExclusive(false);

  powerSwitch3V3.setChecked(true);
  powerSwitch5V.setChecked(true);

  return;
}

void SettingsTab::lockHighPower()
{
  powerSwitchVBat.setState(false);
  powerSwitchVBat.lock();

  powerSwitch12V.setState(false);
  powerSwitch12V.lock();
}

void SettingsTab::lockAllPower()
{
  lockHighPower();

  powerSwitch3V3.setState(false);
  powerSwitch3V3.lock();
  powerSwitch5V.setState(false);
  powerSwitch5V.lock();
}

void SettingsTab::queryBatteryVoltage()
{
  std_msgs::String queryMsg;
  queryMsg.data = "lsbat";
  powerBoardPublisher_.publish(queryMsg);
}

void SettingsTab::powerBoardCb(const std_msgs::String::ConstPtr& msg)
{
  int i = 0;

  //Skip leading zeroes
  while (msg->data[i++] == '\0') {}

  std::string trimmed = msg->data.substr(i-1);

  ROS_INFO("PMB message: %s", trimmed.c_str());
  
  if (trimmed.rfind("ASYNC", 0) == 0)
  {
    handleAsyncMsg(trimmed);
  }
  else if (trimmed.find("BATTERY") != std::string::npos) 
  {
    int adcMeas = parseBatteryLevel(trimmed);
    emit voltageCheck(adcMeas);
  }

}

void SettingsTab::stepperPosCb(const std_msgs::Int32::ConstPtr& msg)
{
  int pos = msg->data;
  topView_->setPixmap(QPixmap::fromImage(*drawTopView(pos)));
}

void SettingsTab::servoPosCb(const std_msgs::Int32::ConstPtr& msg)
{
  int pos = msg->data;
  sideView_->setPixmap(QPixmap::fromImage(*drawSideView(pos)));
}

int SettingsTab::parseBatteryLevel(const std::string& msg)
{
  int currentLoad = 0;
  std::array<int, 4> cellVoltage;

  constexpr int MAX_WORDS = 20;

  std::istringstream ss(msg);

  std::string word;

  for (int i = 0; i < MAX_WORDS && ss >> word; i++) 
  {
    if (i == 0)
    {
      if (word != "OK")
      {
        ROS_WARN_STREAM("Reading battery status returned error!");
        return -1;
      }
    }
    else if (i == 1)
    {
      currentLoad = std::stoi(word);
    }
    else if (i > 1 && i < 6)
    {
      cellVoltage[i-2] = std::stoi(word);
    }
  }

  int *minRead = std::min_element(cellVoltage.begin(), cellVoltage.end());

  if (minRead == nullptr)
    return -1;

  return *minRead;
}

void SettingsTab::handleAsyncMsg(const std::string& asyncMsg)
{
  if (asyncMsg == LOW_VOLTAGE_MESSAGE)
  {
    emit lowVoltage();
    lockHighPower();
  }
  else if (asyncMsg == CRIT_VOLTAGE_MESSAGE)
  {
    emit critVoltage();
    lockAllPower();
  }
  else if (asyncMsg == BAT_NOT_FOUND_MESSAGE)
  {
    static bool oneTime = false;

    if (!oneTime)
    {
      emit noBattery();
      oneTime = true;
    }
  }

  return;
}

} // namespace gui