#include "KeyPress.h"


int main(int argc, char **argv)
{
    QApplication a(argc, argv);

    ros::init(argc, argv, "key_press_publisher");
    ros::NodeHandle nh;
    ros::Publisher keyPublisher = nh.advertise<std_msgs::String>("keys/keyboard", 1);

    auto *keyPress = new gui::KeyPress(keyPublisher);
    keyPress->show();

    return a.exec();
}

