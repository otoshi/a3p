#pragma once

#include "SettingsTab.h"
#include "LiveViewTab.h"

namespace gui
{

class MainGuiWindow : public QWidget
{
  Q_OBJECT
  public:
    explicit MainGuiWindow(QWidget* parent = 0);

    ~MainGuiWindow();


  private slots:

    void showLowVoltageWarn();
    void showCritVoltageWarn();
    void showMissingBatWarn();
    void updateBatteryLevel(int adcMeas);
    void shotEvent();

  private:

    void initTabs();
    void initBatteryLevelBar();
    void initMainLayout();

    void keyPressEvent(QKeyEvent *) override;
    void keyReleaseEvent(QKeyEvent *) override;

    void publishKeyEvent(int key, bool pressed);
    void notifyKeyEvent(int key, bool pressed);
    void notifyKeyEvent(const char* key, bool pressed);

    int mapAdcMeasToVoltage(int adcMeas);

    QProgressBar* batteryBar_ = nullptr;
    QBoxLayout* mainLayout_ = nullptr;
    QTabWidget* tabs_ = nullptr;
    LiveViewTab* lvTab_ = nullptr;
    SettingsTab* stTab_ = nullptr;

    ros::Publisher keyPublisher_;
    ros::AsyncSpinner spinner_;
    ros::NodeHandle nh;
};

} // namespace gui