#include <ros/ros.h>
#include "MainGuiWindow.h"

int main (int argc, char** argv)
{
  ros::init(argc, argv, "qt_frames_listener");
  QApplication a(argc, argv);
  QFile file(":/dark.qss");
  file.open(QFile::ReadOnly | QFile::Text);
  QTextStream s(&file);
  a.setStyleSheet(s.readAll());
  gui::MainGuiWindow window;
  window.show();
  return a.exec();
}