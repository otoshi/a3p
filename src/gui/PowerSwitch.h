#pragma once

#include <memory>
#include <ros/ros.h>
#include "std_msgs/String.h"
#include "std_msgs/Int32.h"
#include <QWidget>
#include <QApplication>
#include <QPushButton>
#include <QLabel>
#include <QLayout>
#include <QTimer>
#include <QCheckBox>
#include <QButtonGroup>
#include <QKeyEvent>
#include <QMainWindow>
#include <QTextStream>
#include <QProgressBar>
#include "CameraWindow.h"
#include "BatteryPopup.h"
#include "imgProc/ImgProcHandler.h"

namespace gui
{

typedef uint8_t PowerSectionsState;

enum class PowerSwitchMask
{
  ALL_OFF     = 0x00,
  SWITCH_3V3  = 0x01,
  SWITCH_5V   = 0x02,
  SWITCH_12V  = 0x04,
  SWITCH_VBAT = 0x08,
  ALL_ON      = 0x0F
};

class PowerSwitch : public QCheckBox
{
  Q_OBJECT

  public:

  explicit PowerSwitch(PowerSwitchMask id, PowerSectionsState& state,
                        PowerSectionsState& cached, ros::Publisher& pub);

  PowerSwitch(const PowerSwitch& ps) = delete;

  void setOn();

  void setOff();

  void lock();

  PowerSectionsState getState();
  
  std_msgs::String getStateMsg();

  public slots:

  void setState(bool checked);

  private:

  QString mapIDtoName(PowerSwitchMask id);
  QString mapIDtoName();

  PowerSwitchMask id_;
  PowerSectionsState& powerBoardStateRef_;
  PowerSectionsState& cachedStateRef_;
  const ros::Publisher& pub_;
};

} // namespace gui