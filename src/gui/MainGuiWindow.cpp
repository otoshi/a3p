#include "MainGuiWindow.h"

namespace gui
{
  enum class BatteryLevels
  {
    DEAD     = 3000,
    CRIT     = 3050,
    MIN      = 3100,
    MAX      = 3500,
    UNKNOWN  = -1,
  };

MainGuiWindow::MainGuiWindow (QWidget* parent)
: QWidget(parent), spinner_(0)
{
  keyPublisher_ = nh.advertise<std_msgs::String>("keys/keyboard", 1);

  initBatteryLevelBar();
  initTabs();
  initMainLayout();
  setLayout(mainLayout_);

  spinner_.start();
}

MainGuiWindow::~MainGuiWindow()
{
}

void MainGuiWindow::initTabs()
{
  lvTab_ = new LiveViewTab(nh, this);
  stTab_ = new SettingsTab(nh, this);
  tabs_= new QTabWidget;
  tabs_->addTab(lvTab_, tr("Live view"));
  tabs_->addTab(stTab_, tr("Settings"));
}

void MainGuiWindow::initBatteryLevelBar()
{
  batteryBar_ = new QProgressBar();
  batteryBar_->setRange(0, 100);
  int nValue = 85;
  

}

void MainGuiWindow::initMainLayout()
{
  mainLayout_ = new QBoxLayout(QBoxLayout::TopToBottom);
  mainLayout_->addWidget(batteryBar_, 0, Qt::AlignTop | Qt::AlignRight);
  mainLayout_->addWidget(tabs_, 0, Qt::AlignTop | Qt::AlignLeft);

  return;
}

void MainGuiWindow::showLowVoltageWarn()
{
  NotifyMessageBox lowVoltagePopup("Battery is low", this);
  lowVoltagePopup.showMessage("Battery is low!", QColor(239, 172, 15), this);
}

void MainGuiWindow::showCritVoltageWarn()
{
  NotifyMessageBox critVoltagePopup("Battery is critical!", this);
  critVoltagePopup.showMessage("Battery is critical!", QColor(180, 15, 15), this);
}

void MainGuiWindow::showMissingBatWarn()
{
  NotifyMessageBox missingBatteryPopup("No baterry detected!", this);
  missingBatteryPopup.showMessage("No baterry detected!", QColor(150, 150, 150), this);
}

void MainGuiWindow::updateBatteryLevel(int adcMeas)
{
  if (adcMeas < 0)
  {
    ROS_WARN_STREAM("Cannot get battery level");
    return;
  }

  // We don't expect battery charging during robot operation.
  
  static int prevMeas = 0;

  if (prevMeas < adcMeas &&
      prevMeas > 0)
  {
    adcMeas = prevMeas;
  }

  int batteryPercent = mapAdcMeasToVoltage(adcMeas);
  
  batteryBar_->setValue(batteryPercent);
  QString myStyleSheet = " QProgressBar::chunk:horizontal {"
  " background-color: ";

  if(adcMeas <= (int) BatteryLevels::CRIT)
  {
      myStyleSheet.append("#bf2719;");
  }
  else if(adcMeas <= (int) BatteryLevels::MIN)
  {
      myStyleSheet.append("#ebc028;");
  }
  else
  {
      myStyleSheet.append("#58b533;");
  }
  myStyleSheet.append("border: 0.1ex transparent; \
                        border-radius: 0.3ex; }");
  batteryBar_->setStyleSheet(myStyleSheet);

  prevMeas = adcMeas;
}

int MainGuiWindow::mapAdcMeasToVoltage(int adcMeas)
{
  if (adcMeas < (int) BatteryLevels::DEAD)
  {
    ROS_WARN_STREAM("ADC reading extends limits: " << adcMeas);
    return -1;
  }

  if (adcMeas > (int) BatteryLevels::MAX)
  {
    adcMeas = (int) BatteryLevels::MAX;
  }

  constexpr double inMin = (double) BatteryLevels::DEAD;
  constexpr double inMax = (double) BatteryLevels::MAX;
  constexpr double outMin = 0, outMax = 100;

  // adc -> % bat
  constexpr double slope = (outMax - outMin) / (inMax - inMin);
  return outMin + slope * (adcMeas - inMin);
}

void MainGuiWindow::publishKeyEvent(int key, bool pressed)
{
  std_msgs::String msg;
  std::stringstream ss;

  //
  // Omit sending button to keyboard listener for local events.
  //

  if (key == Qt::Key_Q && pressed)
  {
    lvTab_->selectObjectToTrack();

    return;
  }
  else if (key == Qt::Key_E && pressed)
  {
    lvTab_->resetObjectToTrack();

    return;
  }
  else if (key == Qt::Key_U && pressed)
  {
    #ifdef USE_NIGHTVISION
    ROS_INFO("Distance measure is disabled in night vision mode.");
    #else
    lvTab_->measureDistanceToTarget();
    #endif

    return;
  }
  // else if (key == Qt::Key_R && pressed && !targetAcquired_)
  // {
  //   ROS_INFO(" Target not acquired. Omit sending button event for r key.");
  //   return;
  // }

  if (pressed)
  {
    ss << "+";
  }
  else
  {
    ss << "-";
  }
  
  ss << (char)key;
  msg.data = ss.str();
  keyPublisher_.publish(msg);

  ROS_INFO("Sending button event: [%s]", msg.data.c_str());

  return;
}

void MainGuiWindow::notifyKeyEvent(int key, bool pressed)
{
  QString text;
  std_msgs::String msg;
  if (pressed)
  {
    text = "You pressed ";
  }
  else
  {
    text = "You released ";
  }
  
  text.append((char)key);

  //lvTab_->printKeyboardInputLabel_->setText(text);

  return;
}

void MainGuiWindow::notifyKeyEvent(const char* key, bool pressed)
{
  QString text;
  std_msgs::String msg;
  if (pressed)
  {
    text = "You pressed ";
  }
  else
  {
    text = "You released ";
  }
  
  text.append(key);

  lvTab_->printKeyboardInputLabel_->setText(text);

  return;
}
 

void MainGuiWindow::keyPressEvent(QKeyEvent* event)
{
  if (event->isAutoRepeat() == false)
  {
    notifyKeyEvent(event->key(), true);
    publishKeyEvent(event->key(), true);
  }
} 

void MainGuiWindow::keyReleaseEvent(QKeyEvent* event)
{
  if (event->isAutoRepeat() == false)
  {
    notifyKeyEvent(event->key(), false);
    publishKeyEvent(event->key(), false);
  }
}

void MainGuiWindow::shotEvent()
{
  notifyKeyEvent("shot button", true);
  publishKeyEvent((int)'R', true);

  return;
}

} // namespace gui