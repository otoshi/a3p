#pragma once

#include <memory>
#include <ros/ros.h>
#include <queue>
#include "std_msgs/String.h"
#include <QWidget>
#include <QApplication>
#include <QPushButton>
#include <QLabel>
#include <QLayout>
#include <QTimer>
#include <QCheckBox>
#include <QButtonGroup>
#include <QKeyEvent>
#include <QMainWindow>
#include "CameraWindow.h"
#include "imgProc/ImgProcHandler.h"

namespace gui
{

struct DistanceMeasInfo
{
  double distPx;
  double distMm;
  double angleDeg;
};

class LiveViewTab : public QWidget
{

  Q_OBJECT

  public:
    explicit LiveViewTab(ros::NodeHandle& node, QWidget* parent = nullptr);

    void selectObjectToTrack();
    void resetObjectToTrack();

    //FIXME: add getter setter
    QLabel* printKeyboardInputLabel_ = nullptr;

  public slots:
    void measureDistanceToTarget();

  protected:
    void resizeEvent(QResizeEvent *) override;

  private slots:
    void updateFrontCamera();
    void updateTurretCamera();
    void streamInterrupted();
    
    void rawView(bool checked);
    void procView(bool checked);
    void noneView(bool checked);

  private:
    void drawObjectsInfo(cv::Mat& frame);
    // Widget setup methods
    void initLiveTabLayout();
    void initCameraWindowsLayout();
    void initFrontCameraLabel();
    void initTurretCameraLabel();
    void initShotButton();
    void initPrintKeyboardInputLabel();  
    void initFpsLabel();
    void initCheckboxes();
    void initTimers(int framerate);
    double pxToMm(double px);
    void printDistanceToTarget();

    enum class previewMode
    {
      RAW,
      PROCESSED,
      NONE,
      MAX_MODES
    };

    previewMode previewMode_ = previewMode::RAW;

    CameraWindow* frontCameraLabel_ = nullptr;
    CameraWindow* turretCameraLabel_ = nullptr;

    QLabel* fpsLabel_ = nullptr;

    QCheckBox* rawImgChBox_ = nullptr;
    QCheckBox* procImgChBox_ = nullptr;
    QCheckBox* noneImgChBox_ = nullptr;

    QButtonGroup* previewModeBoxes_ = nullptr;

    QPushButton* shotButton_ = nullptr;

    ros::NodeHandle& nh;
    ros::Publisher targetCoordPublisher_;
    ros::ServiceClient distMeasClient_;

    QBoxLayout* liveViewTabLayout_ = nullptr;
    QBoxLayout* cameraWindowsLayout_ = nullptr;
    
    CameraFocusHandler cameraFocusHandler_;
    QRect   screenRect_;

    QTimer* frameTimer_ = nullptr;
    QTimer* videoInterruptCheckTimer_ = nullptr;
    QTimer* distMeasTimer_ = nullptr;

    imgProc::ImgProcHandler imgProcHandler_;
    imgProc::ImageConverter turretCameraGrabber_;
    bool interrupted_ = false;
    unsigned int lastFrameIndex_ = 0;
    unsigned int currentFrameIndex_ = 0;

    objVect detectedObjectsInfo_;
    int selectedObjectToTrack_ = -1;
    bool targetAcquired_ = false;

    std::queue<int> pixDists_;
    // DistanceMeasInfo targetDistanceInfo_;

    int prevFrameIndex_ = 0;
};

} // namespace gui