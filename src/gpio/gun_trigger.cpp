#include "ros/ros.h"
#include "std_msgs/String.h"
#include <wiringPi.h>
#include <string>

constexpr int TRIGGER_PIN = 2;
constexpr int RELOAD_DELAY_MS = 200;

const std::string shootKey = "+R";

void shoot()
{
  digitalWrite (TRIGGER_PIN, HIGH); 
  delay(RELOAD_DELAY_MS);
  digitalWrite (TRIGGER_PIN, LOW);
}

void keyEventCallback(const std_msgs::String::ConstPtr& msg)
{
  std::string order = msg->data;
  ROS_DEBUG("I heard: [%s]", order.c_str());

  // TODO: add more safety and targeting conditions here
  if (order == shootKey) // && targetAcquired == true)
  {
    ROS_WARN("Shot!");
    shoot();
  }
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "key_press_listener");
  ros::NodeHandle nh;

  ros::Subscriber keyListener = nh.subscribe("keys/keyboard", 1, keyEventCallback);

  wiringPiSetup();
  pinMode (TRIGGER_PIN, OUTPUT);

  ros::spin();

  return 0;
}
