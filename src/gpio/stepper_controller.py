#!/usr/bin/env python2

import serial
import roslib
import sys
import rospy
import pigpio
from std_msgs.msg import Float32
from std_msgs.msg import Int32
import time
import os
import threading

# pins definitions

STEP_PIN = 12
DIR_PIN = 16
#ENABLE_PIN = 

MANUAL_SPEED_DELAY = 0.0015

class Stepper:

  def __init__(self):
      self.currPos = 0.0
      self.prevPos = 0.0
      self.pi = pigpio.pi()
      self.interrupt = False
      self.driverEnabled = 1
      self.posPub = rospy.Publisher("motor_positions/stepper", Int32, queue_size=1)

  def enableDriver(self):
  #  self.pi.write(ENABLE_PIN, 1)
    self.driverEnabled = 1

  def disableDriver(self):
  #  self.pi.write(ENABLE_PIN, 0)
    self.driverEnabled = 0

  def asyncMove(self, steps, speed, dire):
 #   if self.driverEnabled is 0:
 #     self.enableDriver()
      self.pi.write(DIR_PIN, dire)
      for i in range(steps):
        self.pi.write(STEP_PIN, 1)
        time.sleep(speed)
        self.pi.write(STEP_PIN, 0)
        time.sleep(speed)
 #   if self.driverEnabled is 1:
 #     self.disableDriver()

  def move(self, posDiff):
    destPos = 0
    dire = 0 # cw
    if self.currPos + posDiff < 360 and self.currPos + posDiff >= 0:
      destPos = self.currPos + posDiff
    elif self.currPos + posDiff < 0:
      destPos = self.currPos + posDiff + 360
      #dire = 1 # ccw
    else:
      destPos = self.currPos + posDiff - 360
      #dire = 0 # ccw

    if posDiff < 0:
        dire = 1 #ccw

    print("Curr pos:", self.currPos)
    print("posDiff: ", posDiff)
    print("dir: ", dire)

    #moveThread = threading.Thread(name="moveThread", target = self.asyncMove, args = ((abs(int (posDiff / 0.9))),0.003,dire,))
    #moveThread.start()
    #moveThread.join()

    self.asyncMove(abs(int(posDiff/0.9)), 0.0015, dire)

    self.prevPos = self.currPos
    self.currPos = int(destPos)

    print("Curr pos: ", self.currPos)
    print("Prev pos: ", self.prevPos)
    self.posPub.publish(self.currPos)

  def getCurrPos(self):
    return self.currPos

  def getPrevPos(self):
    return self.prevPos

  def callback(self, data):
      rospy.loginfo(rospy.get_caller_id() + "Move stepper %f deg", data.data)
      self.move(float(data.data))

  def callbackInf(self, data):
      if data.data is 0:
        rospy.loginfo(rospy.get_caller_id() + "Stop moving, data: %i", data.data)
        self.interrupt = True
      else:
        self.interrupt = False
        rospy.loginfo(rospy.get_caller_id() + "Move infinite")
        dire = 1
        if data.data is -1:
          dire = 0
        moveThreadInf = threading.Thread(name="moveThreadInf", target = self.asyncMoveInf, args = ((dire),))
        moveThreadInf.start()

  def asyncMoveInf(self, dire):
    stepsDone = 0.0

    if dire is 0:
      isDirCw = 1
    else:
      isDirCw = -1
    self.pi.write(DIR_PIN, dire)
    while self.interrupt is False:
      self.pi.write(STEP_PIN, 1)
      time.sleep(MANUAL_SPEED_DELAY)
      self.pi.write(STEP_PIN, 0)
      time.sleep(MANUAL_SPEED_DELAY)
      stepsDone += 1

      diffPos = (stepsDone * 0.9) * isDirCw / 10

      self.currPos = (self.currPos + diffPos) % 360
      self.posPub.publish(self.currPos)


def main(args):

  stepper = Stepper()

  rospy.init_node("stepper_controller", anonymous=True)
  rospy.Subscriber("move_orders/stepper", Float32, stepper.callback)
  rospy.Subscriber("move_orders/stepper_manual", Int32, stepper.callbackInf)

  rospy.spin()

if __name__ == '__main__':
  main(sys.argv)
