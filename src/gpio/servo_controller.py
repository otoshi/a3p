#!/usr/bin/env python2
from __future__ import print_function

import roslib
import sys
import rospy
from std_msgs.msg import Float32, Int32
import pigpio
import time
import os
import threading

SERVO_PWM_PIN  = 12
SERVO_PWM_FREQ = 45
SERVO_PRESCALER = 10000

class Servo:
  maxPW = 12
  minPW = 3

  minDeg = 0
  maxDeg = 40

  interrupt = 0

  prevPos = 0.0
  currPos = 0.0

  posPub = rospy.Publisher("motor_positions/servo", Int32, queue_size=1)

  def __init__(self, pi):
      self.pi = pi


  def move(self, diffPos):

    newPos = diffPos + self.currPos

    if newPos < self.minDeg or newPos > self.maxDeg:
      rospy.loginfo("Desired position value exceeds servo soft limits")
      return -1.0
    
    pulseWidth = self.mapDegToPulseWidth(newPos)

    if pulseWidth <= self.maxPW and pulseWidth >= self.minPW:

      self.pi.hardware_PWM(SERVO_PWM_PIN, SERVO_PWM_FREQ, pulseWidth * SERVO_PRESCALER)

      self.prevPos = self.currPos
      # TODO: increase precision to 0. deg
      self.currPos = int(newPos)

      rospy.loginfo("Curent servo pos: %i", self.currPos)

      self.posPub.publish(self.currPos)
      return self.currPos

    else:
      rospy.logwarn("Desired position value exceeds servo hard limits")
      return -1.0

  def getCurrPos(self):
    return self.currPos

  def getPrevPos(self):
    return self.prevPos

  def callback(self, data):
      rospy.loginfo(rospy.get_caller_id() + "Move servo %s deg", data.data)
      self.move(float(data.data))

  def callbackManual(self, data):
      if data.data == 0:
        self.interrupt = 1
      else:
        self.interrupt = 0
        moveThreadInf = threading.Thread(name="moveThreadInf", target = self.asyncMoveInf, args = ((data.data),))
        moveThreadInf.start()

  def asyncMoveInf(self, pos):
     while self.interrupt is 0:
        self.move(pos)
     #self.move(-2)

  def mapDegToPulseWidth(self, value):
      leftMin = 0
      leftMax = 180
      rightMin = self.minPW
      rightMax = self.maxPW
      
      # Figure out how 'wide' each range is
      leftSpan = leftMax - leftMin
      rightSpan = rightMax - rightMin

      # Convert the left range into a 0-1 range (float)
      valueScaled = float(value - leftMin) / float(leftSpan)

      # Convert the 0-1 range into a value in the right range.
      result = rightMin + (valueScaled * rightSpan)
      rospy.logdebug("%f deg -> %f PW", value, result)

      return result

def main(args):

  rospy.init_node("servo_controller", anonymous=True)
  rospy.loginfo("Servo controller started on pin %i", SERVO_PWM_PIN)

  pi = pigpio.pi()

  servo = Servo(pi)

  servo.move(0)

  rospy.Subscriber("move_orders/servo", Float32, servo.callback)
  rospy.Subscriber("move_orders/servo_manual", Int32, servo.callbackManual)

  rospy.spin()


if __name__ == '__main__':
    main(sys.argv)
