#!/usr/bin/env python2

import serial
import roslib
import sys
import rospy
from threading import Thread, Lock
from std_msgs.msg import Float32
from std_msgs.msg import String
import time
import os

# TODO: use serial.threaded.ReaderThread to handle async msgs

maxMsgLen = 16

serialMutex = Lock()

boardInitialized = False

def setBoardInit(value):
  global boardInitialized
  serialMutex.acquire()
  boardInitialized = value
  serialMutex.release()

def getBoardInit():
  serialMutex.acquire()
  value = boardInitialized
  serialMutex.release()
  return value

def resetBoardRxBuffer(ser):
  global boardInitialized
  while getBoardInit() is False:
    ser.write('\n'.encode())
    time.sleep(0.05)

def sendToSerial(order, ser):

  orderLen = len(order)

  if orderLen > maxMsgLen:
    rospy.logwarn(rospy.get_caller_id() + "ERROR! len order > 16", data.data)
  else:
    missingChars = maxMsgLen - orderLen
    order += "\r" * missingChars

  print "Order: ", order
  try:
    ser.write(order.encode())
  except serial.SerialException, serial.SerialTimeoutException:
    return

class PowerBoard:
  def __init__(self, serial):
      self.serial = serial

  def callback(self, data):
      rospy.loginfo(rospy.get_caller_id() + "Power board order %s", data.data)
      sendToSerial(data.data, self.serial)

def readStdin(ser):
  print "Listening to stdin started"
  while not rospy.is_shutdown():
    try:
        line = sys.stdin.readline()
        print "Stdin order: ", line
        sendToSerial(line, ser)
    except KeyboardInterrupt:
        break

def readSerialInput(ser, pub):
  print("Reading serial input started.")
  global boardInitialized
  while not rospy.is_shutdown():
    try:
      retMsg = ser.readline()
      if getBoardInit() is False:
        setBoardInit(True)
      print "Read response: ", retMsg
      pub.publish(str(retMsg))
    except serial.SerialException, serial.SerialTimeoutException:
        break

def main(args):

  SERIAL_PORT = "/dev/ttyUSB0"
  BAUDRATE = 115200

  try:
    ser = serial.Serial(SERIAL_PORT, BAUDRATE)

    ser.bytesize = serial.EIGHTBITS

    ser.parity = serial.PARITY_NONE

    ser.stopbits = serial.STOPBITS_ONE

    if ser.isOpen():

      ser.flushInput()
      ser.flushOutput()

      pb = PowerBoard(ser)

      print("Starting ros node")
      rospy.init_node("power_board_controller", anonymous=True)
      rospy.Subscriber("power_board/orders", String, pb.callback)
      pub = rospy.Publisher("power_board/responses", String, queue_size=10)

      serialInputThr = Thread(name="serialInputThread", target = readSerialInput, args = ((ser),(pub),))
      serialInputThr.start()

      resetBoardRxBuffer(ser)

      stdinThr = Thread(name="stdinThread", target = readStdin, args = ((ser),))
      
      stdinThr.start()
      
      rospy.spin()

      ser.close()
      raise KeyboardInterrupt
      stdinThr.join()
      serialInputThr.join()

    else:
        print("Cannot open serial port")

  except serial.SerialException as e:
    print ("Error open serial port: " + str(e))
    exit()
  except KeyboardInterrupt:
    print "Bye bye"
    os._exit(1)


if __name__ == '__main__':
  main(sys.argv)
