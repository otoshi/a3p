#!/usr/bin/env python

from __future__ import print_function

from a3p.srv import LaserToggle, LaserToggleResponse
import rospy
import pigpio

lLaserState = 0
rLaserState = 0

pi = pigpio.pi()

LEFT_LASER_PIN = 23
RIGHT_LASER_PIN = 24

def setLaserState(req):

  global lLaserState
  global rLaserState

  if req.lLaserRequest != lLaserState:
    pi.write(LEFT_LASER_PIN, req.lLaserRequest)
    lLaserState = req.lLaserRequest

  if req.rLaserRequest != rLaserState:
    pi.write(RIGHT_LASER_PIN, req.rLaserRequest)
    rLaserState = req.rLaserRequest

  print("Returning [left laser: %s right laser: %s]"%(req.lLaserRequest, req.rLaserRequest))
  return LaserToggleResponse(lLaserState, rLaserState)

def laserToggleServer():
  rospy.init_node('laser_toggle_srv')
  s = rospy.Service('laser_toggle', LaserToggle, setLaserState)
  print("Laser toggle server ready.")
  rospy.spin()

if __name__ == "__main__":
  laserToggleServer()
