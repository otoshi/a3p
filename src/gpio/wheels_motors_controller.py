#!/usr/bin/env python2
from __future__ import print_function

import roslib
import sys
import rospy
from std_msgs.msg import Int32
import pigpio
import time

class Wheel:

  def __init__(self, out, pin_1, pin_2):
    self.pi   = out
    self.pin_1 = pin_1
    self.pin_2 = pin_2

  def move_fwd(self):
    self.pi.write(self.pin_1, 1)
    self.pi.write(self.pin_2, 0)

  def move_bwd(self):
    self.pi.write(self.pin_1, 0)
    self.pi.write(self.pin_2, 1)
  
  def stop_block(self):
    self.pi.write(self.pin_1, 1)
    self.pi.write(self.pin_2, 1)

  def stop_release(self):
    self.pi.write(self.pin_1, 0)
    self.pi.write(self.pin_2, 0)

class Controller:
  def __init__(self, out):
    self.pi = out
    self.wheel_FL = Wheel(self.pi, 6, 13)
    self.wheel_FR = Wheel(self.pi, 4, 17)
    self.wheel_RL = Wheel(self.pi, 19, 26)
    self.wheel_RR = Wheel(self.pi, 27, 22)
    self.wheels = [self.wheel_FL, self.wheel_FR, self.wheel_RL, self.wheel_RR]

  def stop_block(self):
    rospy.loginfo("stop release")
    for wheel in self.wheels:
      wheel.stop_block()

  def stop_release(self):
    rospy.loginfo("stop release")
    for wheel in self.wheels:
      wheel.stop_release()

  def move_fwd(self):
    rospy.loginfo("move forward")
    for wheel in self.wheels:
      wheel.move_fwd()

  def move_bwd(self):
    rospy.loginfo("move backward")
    for wheel in self.wheels:
      wheel.move_bwd()

  def move_left(self):
    rospy.loginfo("move left")
    self.wheel_FR.move_fwd()
    self.wheel_RR.move_fwd()
    self.wheel_FL.move_bwd()
    self.wheel_RL.move_bwd()

  def move_right(self):
    rospy.loginfo("move right")
    self.wheel_FL.move_fwd()
    self.wheel_RL.move_fwd()
    self.wheel_FR.move_bwd()
    self.wheel_RR.move_bwd()

  def callback(self, msg):
    rospy.loginfo("I heard %s", msg.data)
    if msg.data == 0:
      self.stop_release()
    elif msg.data == 1:
      self.move_fwd()
    elif msg.data == 2:
      self.move_bwd()
    elif msg.data == 3:
      self.move_left()
    elif msg.data == 4:
      self.move_right()

def main(args):

  controller = Controller(pigpio.pi())

  rospy.loginfo("Wheels motors controller started.")

  rospy.init_node("wheels_motors_controller", anonymous=True)
  rospy.Subscriber("move_orders/wheels_manual", Int32, controller.callback)

  rospy.spin()

if __name__ == '__main__':
    main(sys.argv)