#!/usr/bin/env python2
from __future__ import print_function

import roslib
import sys
import rospy
from std_msgs.msg import String
import pigpio
import time

class Trigger:

  triggerPin = 25
  reloadDelayS = 0.10

  def __init__(self, pi):
    self.pi = pi

  def keyEventCallback(self, msg):
    rospy.loginfo("I heard %s", msg.data)
    if msg.data == "+R":
      rospy.logwarn("Shot!")
      self.pi.write(self.triggerPin, 1)
      time.sleep(self.reloadDelayS)
      self.pi.write(self.triggerPin, 0)

def main(args):

  trigger = Trigger(pigpio.pi())

  rospy.loginfo("Gun trigger started on pin %i", trigger.triggerPin)

  rospy.init_node("servo_controller", anonymous=True)
  rospy.Subscriber("keys/keyboard", String, trigger.keyEventCallback)

  rospy.spin()

if __name__ == '__main__':
    main(sys.argv)