#pragma once

#include <vector>
#include <utility>
#include <opencv2/imgproc/imgproc.hpp>

typedef std::vector <std::pair<const char*, cv::Rect2i>> objVect;