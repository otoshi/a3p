#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Int32.h"
#include "a3p/LaserToggle.h"

//
// Stepper cw ccw movement
//

#define MOVE_CW_BUTTON_PRESS "+J"
#define MOVE_CW_BUTTON_RELEASE "-J"
#define MOVE_CCW_BUTTON_PRESS "+L"
#define MOVE_CCW_BUTTON_RELEASE "-L"

//
// Servo up and down movement
//

#define MOVE_UP_BUTTON_PRESS "+I"
#define MOVE_UP_BUTTON_RELEASE "-I"
#define MOVE_DOWN_BUTTON_PRESS "+K"
#define MOVE_DOWN_BUTTON_RELEASE "-K"

//
// Wheels movement
//

#define MOVE_FORWARD_BUTTON_PRESS "+W"
#define MOVE_FORWARD_BUTTON_RELEASE "-W"
#define MOVE_BACKWARD_BUTTON_PRESS "+S"
#define MOVE_BACKWARD_BUTTON_RELEASE "-S"
#define MOVE_LEFT_BUTTON_PRESS "+A"
#define MOVE_LEFT_BUTTON_RELEASE "-A"
#define MOVE_RIGHT_BUTTON_PRESS "+D"
#define MOVE_RIGHT_BUTTON_RELEASE "-D"

//
// Lasers
//

#define LASER_LEFT_TOGGLE  "+O"
#define LASER_RIGHT_TOGGLE "+P"

enum class OutputType
{
  NONE,
  STEPPER,
  SERVO,
  DC_WHEELS,
  LASER,
};

class KeyPressHandler
{
  public:

  KeyPressHandler();

  private:

  void keyEventCallback(const std_msgs::String::ConstPtr& msg)
  {
    ROS_INFO("I heard: [%s]", msg->data.c_str());
    handleKeyPress(msg->data);
  }
  void handleKeyPress(const std::string msg);

  ros::NodeHandle nh;
  ros::Publisher  manualStepperPub;
  ros::Publisher  manualServoPub;
  ros::Publisher  manualWheelsPub;
  ros::Publisher  laserTogglePub;
  ros::Subscriber keyListener;

  ros::ServiceClient laserToggleClient;
  a3p::LaserToggle laserToggleMessage;
  int lLaserState = 0;
  int rLaserState = 0;

};

KeyPressHandler::KeyPressHandler()
{
  //TODO: change it to service
  manualStepperPub = nh.advertise<std_msgs::Int32>("move_orders/stepper_manual", 1);
  manualServoPub   = nh.advertise<std_msgs::Int32>("move_orders/servo_manual", 1);
  manualWheelsPub  = nh.advertise<std_msgs::Int32>("move_orders/wheels_manual", 1);
  
  laserToggleClient = nh.serviceClient<a3p::LaserToggle>("laser_toggle");
  
  keyListener = nh.subscribe("keys/keyboard", 1, 
                                &KeyPressHandler::keyEventCallback, this);
}

void KeyPressHandler::handleKeyPress(const std::string msg)
{
  std_msgs::Int32 order;
  OutputType output = OutputType::NONE;

  if (msg.compare(MOVE_CW_BUTTON_PRESS) == 0)
  {
    order.data = 1;
    output = OutputType::STEPPER;
  }
  else if (msg.compare(MOVE_CW_BUTTON_RELEASE) == 0)
  {
    order.data = 0;
    output = OutputType::STEPPER;
  }
  else if (msg.compare(MOVE_CCW_BUTTON_PRESS) == 0)
  {
    order.data = -1;
    output = OutputType::STEPPER;
  }
  else if (msg.compare(MOVE_CCW_BUTTON_RELEASE) == 0)
  {
    order.data = 0;
    output = OutputType::STEPPER;
  }
  else if (msg.compare(MOVE_UP_BUTTON_PRESS) == 0)
  {
    order.data = 1;
    output = OutputType::SERVO;
  }
  else if (msg.compare(MOVE_UP_BUTTON_RELEASE) == 0)
  {
    order.data = 0;
    output = OutputType::SERVO;
  }
  else if (msg.compare(MOVE_DOWN_BUTTON_PRESS) == 0)
  {
    order.data = -1;
    output = OutputType::SERVO;
  }
  else if (msg.compare(MOVE_DOWN_BUTTON_RELEASE) == 0)
  {
    order.data = 0;
    output = OutputType::SERVO;
  }
  else if (msg.compare(MOVE_FORWARD_BUTTON_PRESS) == 0)
  {
    order.data = 1;
    output = OutputType::DC_WHEELS;
  }
  else if (msg.compare(MOVE_BACKWARD_BUTTON_PRESS) == 0)
  {
    order.data = 2;
    output = OutputType::DC_WHEELS;
  }
  else if (msg.compare(MOVE_LEFT_BUTTON_PRESS) == 0)
  {
    order.data = 3;
    output = OutputType::DC_WHEELS;
  }
  else if (msg.compare(MOVE_RIGHT_BUTTON_PRESS) == 0)
  {
    order.data = 4;
    output = OutputType::DC_WHEELS;
  }
  else if (msg.compare(MOVE_FORWARD_BUTTON_RELEASE) == 0 ||
           msg.compare(MOVE_BACKWARD_BUTTON_RELEASE) == 0 ||
           msg.compare(MOVE_LEFT_BUTTON_RELEASE) == 0 ||
           msg.compare(MOVE_RIGHT_BUTTON_RELEASE) == 0)
  {
    order.data = 0;
    output = OutputType::DC_WHEELS;
  }
  else if (msg.compare(LASER_LEFT_TOGGLE) == 0)
  {
    lLaserState = !lLaserState;
    output = OutputType::LASER;
  }
  else if (msg.compare(LASER_RIGHT_TOGGLE) == 0)
  {
    rLaserState = !rLaserState;
    output = OutputType::LASER;
  }
  else
  {
    ROS_DEBUG("Unknown button message");
    order.data = 0;
  }

  if (output == OutputType::STEPPER)
  {
    manualStepperPub.publish(order);
  }
  else if (output == OutputType::SERVO)
  {
    manualServoPub.publish(order);
  }
  else if (output == OutputType::DC_WHEELS)
  {
    manualWheelsPub.publish(order);
  }
  else if (output == OutputType::LASER)
  {
    laserToggleMessage.request.lLaserRequest = lLaserState;
    laserToggleMessage.request.rLaserRequest = rLaserState;

    if (laserToggleClient.call(laserToggleMessage))
    {
      ROS_INFO("Lasers set to state: %i %i", 
      laserToggleMessage.response.rLaserResponse, laserToggleMessage.response.lLaserResponse);
    }
    else
    {
      ROS_ERROR("Failed to call service laser_toggle");
    }
  }

  return;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "key_press_listener");
  
  KeyPressHandler keyPressHandler;

  ros::spin();

  return 0;
}
