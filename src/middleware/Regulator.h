#pragma once

#include "std_msgs/String.h"
#include "std_msgs/Float32.h"
#include <ros/ros.h>

class MotorRegulator
{
  public:

  MotorRegulator();
  virtual ~MotorRegulator(){}

  virtual const char* getName(){return "Motor";}

  protected:

  ros::NodeHandle nh_;
  ros::Publisher publisher_;
  ros::Subscriber subscriber_;

  std_msgs::Float32 angle_;

  struct Point2i
  {
    int x;
    int y;
  };

  Point2i objectCenter_ = {0,0};
  Point2i frameCenter_ = {0,0};
  double sleepDuration_ = 0.1;

  void callback (const std_msgs::String::ConstPtr& msg);
  virtual double pixDifftoMotorAngle(){};
};


//
// IR cut camera has 40 deg view angle.
// Image has 640 x 360 px.
// Center px coords: O(320,160)
// Max horizontal diff is though +- 320
// So for max diff move 40 deg horizontally
// So 8 px is 1 deg of move.
// Max vertical diff is +- 160, but the turret can only move up,
// so vert diff has (0,+160) range
// for 160 move 40 deg
// so 4 px is 1 deg of move
//
// At first shot divide each coeff by 4 
// to make smaller more frequent steps
//

class ServoRegulator : public MotorRegulator
{
  public:

  ServoRegulator();

  virtual const char* getName(){return "Servo";}

  protected:
  double pixDifftoMotorAngle() override;

  double magicCoeff_ = 0.25/4;
};

class StepperRegulator : public MotorRegulator
{
  public:

  StepperRegulator();
  
  virtual const char* getName(){return "Stepper";}

  protected:
  double pixDifftoMotorAngle() override;

  double magicCoeff_ = 0.125/4;
};
