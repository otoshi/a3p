#include "Regulator.h"

int main(int argc, char **argv)
{
  ros::init(argc, argv, "stepper_regulator");

  StepperRegulator stepper;

  ros::spin();

  return 0;
}
