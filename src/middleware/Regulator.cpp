#include "Regulator.h"

MotorRegulator::MotorRegulator()
{
  subscriber_ = nh_.subscribe("coords/target", 1, &MotorRegulator::callback, this);

  angle_.data = 0.0;
}

ServoRegulator::ServoRegulator()
{
  publisher_ = nh_.advertise<std_msgs::Float32>("move_orders/servo", 1);
	sleepDuration_ = 0.5;
}

StepperRegulator::StepperRegulator()
{
  publisher_ = nh_.advertise<std_msgs::Float32>("move_orders/stepper", 1);
	sleepDuration_ = 0.15;
}

void MotorRegulator::callback (const std_msgs::String::ConstPtr& msg)
{
  ROS_INFO("%s regulator: [%s]", getName(), msg->data.c_str());
  sscanf(msg->data.c_str(), "%i %i %i %i", 
            &objectCenter_.x, &objectCenter_.y,
                &frameCenter_.x, &frameCenter_.y);

	angle_.data = pixDifftoMotorAngle();

  ROS_INFO("%s angle %f", getName(), angle_.data);

  publisher_.publish(angle_);
  
  ros::Duration(sleepDuration_).sleep();
}

double ServoRegulator::pixDifftoMotorAngle()
{
  int diff = frameCenter_.y - objectCenter_.y;
  
  double angle = diff * magicCoeff_;

  constexpr int maxAngle = 5;

  if (angle <= 1 && angle >= -1)
  {
    angle = 0;
  }

  if (angle < -maxAngle)
  {
    angle = -maxAngle;
  }
  else if (angle > maxAngle)
  {
    angle = maxAngle;
  }

  ROS_INFO_STREAM("Diff: " << diff << " " << getName() << " angle: " << angle);

  return angle;
}

double StepperRegulator::pixDifftoMotorAngle()
{
	int diff = -(frameCenter_.x - objectCenter_.x);

  double angle = diff * magicCoeff_;

  constexpr int maxAngle = 10;

  if (angle < -maxAngle)
  {
    angle = -maxAngle;
  }
  else if (angle > maxAngle)
  {
    angle = maxAngle;
  }
  
  ROS_INFO_STREAM("Diff: " << diff << " " << getName() << " angle: " << angle);

  return angle;
}