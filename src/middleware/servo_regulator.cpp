#include "Regulator.h"

int main(int argc, char **argv)
{
  ros::init(argc, argv, "servo_regulator");

  ServoRegulator servo;

  ros::spin();

  return 0;
}
