#include "Regulator.h"

int main(int argc, char **argv)
{
  ros::init(argc, argv, "drives_regulator");

  Regulator reg;

  ros::spin();

  return 0;
}
