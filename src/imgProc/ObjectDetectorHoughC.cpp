#include "ObjectDetectorHoughC.h"

namespace imgProc
{

ObjectDetectorHoughC::ObjectDetectorHoughC() 
{

}

ObjectDetectorHoughC::~ObjectDetectorHoughC() 
{

}

objVect ObjectDetectorHoughC::detect(cv::Mat& frame)
{
  if(frame.empty())
  {
    throw std::runtime_error("Frame is empty");
  }

  detectedObjects_.clear();

  cv::Mat gray;
  cv::cvtColor(frame, gray, cv::COLOR_BGR2GRAY);
  cv::medianBlur(gray, gray, 5);
  std::vector<cv::Vec3f> circles;
  cv::HoughCircles(gray, circles, cv::HOUGH_GRADIENT,
                   1, // dp
                   20,  // minDist
                   50, // high Canny 
                   30,  // detector sensitivity
                   10,   // min radi
                   60); // max radi

  for (size_t i = 0; i < circles.size(); i++ )
  {
    cv::Vec3i c = circles[i];
    int& x = c[0];
    int& y = c[1];
    int& r = c[2];
    detectedObjects_.push_back(std::pair<const char*, cv::Rect2i>("circle", cv::Rect2i(x-r, y-r, 2*r, 2*r)));
  }
  
  return detectedObjects_;
}

} // namespace imgProc