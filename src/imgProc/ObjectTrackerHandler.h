#pragma once
#include "ObjectMultiTracker.h"
#include "util/Types.h"
#include <iostream>
#include <string_view>

namespace imgProc
{

class ObjectTrackerHandler
{
  public:

  enum class TrackerType;

  ObjectTrackerHandler();
  explicit ObjectTrackerHandler(TrackerType type);
  
  bool update(cv::Mat& frame, objVect& objects);
  void refresh(cv::Mat& frame, objVect detectedObjects);
  int setTrackerType(std::string_view type);
  void setTrackedObject(int* trackedObjectIndex);

  enum class TrackerType
  {
    BOOSTING,
    MIL,
    KCF,
    TLD,    
    MEDIANFLOW,
    GOTURN,
    MOSSE,
    CSRT
  };

  private:

  std::string_view trackerType_ = "CSRT";
  std::vector<std::string> trackerTypes_ = 
          {"BOOSTING", "MIL", "KCF", "TLD", 
          "MEDIANFLOW", "GOTURN", "MOSSE", "CSRT"};

  cv::Ptr<ObjectMultiTracker> tracker_;

  cv::Ptr<cv::Tracker> createTrackerByName(std::string_view trackerType);

};

} // namespace imgProc