#pragma once

#include "ObjectDetector.h"
#include <string>
#include <vector>

namespace imgProc
{

class ObjectDetectorYOLO : public ObjectDetector
{

public:

  ObjectDetectorYOLO();
  virtual ~ObjectDetectorYOLO();

  objVect detect(cv::Mat& frame);

protected:

void postprocess(cv::Mat& frame, const std::vector<cv::Mat>& outs);

void drawPred(int classId, float conf, int left, int top, int right, int bottom, cv::Mat& frame);

std::vector<cv::String> getOutputsNames();

};

} // namespace imgProc