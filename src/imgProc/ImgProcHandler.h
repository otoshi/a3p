#pragma once
#include <memory>
#include <opencv2/imgproc/imgproc.hpp>
#include "ImageConverter.h"
#include "ObjectDetector.h"
#include "ObjectTrackerHandler.h"

namespace imgProc
{

class ImgProcHandler
{
  public:

  ImgProcHandler();
  objVect detectObjects(cv::Mat& frame);
  cv::Mat& getRawImg();
  int getFrameIndex();
  void setTrackedObject(int* objectIndex);

  private:

  objVect detected_;

  std::unique_ptr<ImageConverter> ic_ = nullptr;
  std::unique_ptr<ObjectDetector> detector_ = nullptr;
  std::unique_ptr<ObjectTrackerHandler> tracker_ = nullptr;

  int framesAfterLastDetection = 0;

  unsigned int prevFrameIndex_ = 0;
};

} // namespace imgProc