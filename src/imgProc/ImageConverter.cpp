#include "ImageConverter.h"

namespace imgProc
{

ImageConverter::ImageConverter(const char* topic) : it_(_nh)
{
  image_sub_ = it_.subscribe(topic, 1, &ImageConverter::imageCb, this);
}

ImageConverter::~ImageConverter()
{
  if (windowExists)
  {
    cv::destroyWindow(WINDOW_NAME);
  }
}

void ImageConverter::imageCb(const sensor_msgs::ImageConstPtr& msg)
{
  try
  {
    _img = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8)->image;
  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
  }   

  frameIndex_++;
}

cv::Mat& ImageConverter::getImg(void)
{
  return _img;
}

int ImageConverter::getFrameIndex()
{
  return frameIndex_;
}

void ImageConverter::createCvWindow(void)
{
  windowExists = true;
  cv::namedWindow(WINDOW_NAME);
}

void ImageConverter::showImg(void)
{
  if (!windowExists)
    createCvWindow();
  if (!_img.empty())
  {
    cv::imshow(WINDOW_NAME, _img);
    cv::waitKey(1);
  }
}

} // namespace imgProc