#pragma once

#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

namespace imgProc
{

class ImageConverter
{
private:
  const char* const WINDOW_NAME = "raw_image";
  bool windowExists = false; 
  long long unsigned int frameIndex_ = 0;

  ros::NodeHandle _nh;
  image_transport::Subscriber image_sub_;
  image_transport::ImageTransport it_;

protected:
  cv::Mat _img;
  void createCvWindow();

public:
  void imageCb(const sensor_msgs::ImageConstPtr&);
  void showImg(void);
  cv::Mat& getImg(void);
  int getFrameIndex();

  explicit ImageConverter(const char* topic);
  virtual ~ImageConverter();
};

} // namespace imgProc