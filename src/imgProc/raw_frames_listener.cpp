#include <ros/ros.h>
#include "ImageConverter.h"

int main (int argc, char** argv)
{
  ros::init(argc, argv, "raw_frames_listener");
  imgProc::ImageConverter ic("raw_frames");
  int fps = 32;
  if (ros::param::has("framerate"))
  {
    ros::param::get("framerate", fps);
    ROS_INFO_ONCE("Ros listener started with set framerate: %i", fps);
  }
  else
  {
    ROS_INFO_ONCE("Ros listener started with default framerate: %i", fps);
  }
  ros::Rate frame_rate(fps);
  while (ros::ok())
  {
    cv::Mat frame = ic.getImg();
    if (!frame.empty())
    {
      cv::imshow("Raw live view", frame);
      cv::waitKey(1);
    }
    ros::spinOnce();
    
    //FIXME: change this to diff between current time and elapsed time
    frame_rate.sleep();
  }
  return 0;
}


