#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <stdlib.h>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/dnn.hpp>
#include "util/Types.h"

#define MAX_DETECTED_OBJECTS 64

namespace imgProc
{

class ObjectDetector
{

public:

  virtual ~ObjectDetector();

  virtual objVect detect(cv::Mat& frame) = 0;

protected:
  ObjectDetector();
  virtual void setNetBackend(); 

  size_t inWidth_ = 300;
  size_t inHeight_ = 300;
  const double inScaleFactor_ = 1.0/127.5;
  const float confidenceThreshold_ = 0.7;
  const cv::Scalar meanVal = cv::Scalar(127.5, 127.5, 127.5);

  const char* configFile_ = nullptr;
  const char* weightFile_ = nullptr;
  std::vector<std::string> classes_;
  cv::dnn::Net net_;
  cv::Mat result_;
  objVect detectedObjects_;

};

} // namespace imgProc