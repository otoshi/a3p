#include "ObjectDetectorYOLO.h"

namespace imgProc
{
using namespace std;
using namespace cv;

//
// TODO: Complete detectObjects method to return proper value
//

// Remove the bounding boxes with low confidence using non-maxima suppression
void ObjectDetectorYOLO::postprocess(Mat& frame, const vector<Mat>& outs)
{
    vector<int> classIds;
    vector<float> confidences;
    vector<Rect> boxes;
    
    for (size_t i = 0; i < outs.size(); ++i)
    {
        // Scan through all the bounding boxes output from the network and keep only the
        // ones with high confidence scores. Assign the box's class label as the class
        // with the highest score for the box.
        float* data = (float*)outs[i].data;
        for (int j = 0; j < outs[i].rows; ++j, data += outs[i].cols)
        {
            Mat scores = outs[i].row(j).colRange(5, outs[i].cols);
            Point classIdPoint;
            double confidence;
            // Get the value and location of the maximum score
            minMaxLoc(scores, 0, &confidence, 0, &classIdPoint);
            if (confidence > confidenceThreshold_)
            {
                int centerX = (int)(data[0] * frame.cols);
                int centerY = (int)(data[1] * frame.rows);
                int width = (int)(data[2] * frame.cols);
                int height = (int)(data[3] * frame.rows);
                int left = centerX - width / 2;
                int top = centerY - height / 2;
                
                classIds.push_back(classIdPoint.x);
                confidences.push_back((float)confidence);
                boxes.push_back(Rect(left, top, width, height));
            }
        }
    }
    
    // Perform non maximum suppression to eliminate redundant overlapping boxes with
    // lower confidences
    vector<int> indices;
    float nmsThreshold = 0.4;
    cv::dnn::NMSBoxes(boxes, confidences, confidenceThreshold_, nmsThreshold, indices);
    for (size_t i = 0; i < indices.size(); ++i)
    {
        int idx = indices[i];
        Rect box = boxes[idx];
        drawPred(classIds[idx], confidences[idx], box.x, box.y,
                 box.x + box.width, box.y + box.height, frame);
    }
}

// Draw the predicted bounding box
void ObjectDetectorYOLO::drawPred(int classId, float conf, int left, int top, int right, int bottom, Mat& frame)
{
    //Draw a rectangle displaying the bounding box
    rectangle(frame, Point(left, top), Point(right, bottom), Scalar(255, 178, 50), 3);
    
    //Get the label for the class name and its confidence
    string label = format("%.2f", conf);
    if (!classes_.empty())
    {
        CV_Assert(classId < (int)classes_.size());
        label = classes_[classId] + ":" + label;
    }
    
    //Display the label at the top of the bounding box
    int baseLine;
    Size labelSize = getTextSize(label, FONT_HERSHEY_SIMPLEX, 0.5, 1, &baseLine);
    top = max(top, labelSize.height);
    rectangle(frame, Point(left, top - round(1.5*labelSize.height)), Point(left + round(1.5*labelSize.width), top + baseLine), Scalar(255, 255, 255), FILLED);
    putText(frame, label, Point(left, top), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(0,0,0),1);
}

// Get the names of the output layers
vector<String> ObjectDetectorYOLO::getOutputsNames()
{
    static vector<String> names;
    if (names.empty())
    {
        //Get the indices of the output layers, i.e. the layers with unconnected outputs
        vector<int> outLayers = net_.getUnconnectedOutLayers();
        
        //get the names of all the layers in the network
        vector<String> layersNames = net_.getLayerNames();
        
        // Get the names of the output layers in names
        names.resize(outLayers.size());
        for (size_t i = 0; i < outLayers.size(); ++i)
        names[i] = layersNames[outLayers[i] - 1];
    }
    return names;
}

ObjectDetectorYOLO::ObjectDetectorYOLO() {

  inWidth_ = 416;
  inHeight_= 416;

  //TODO: change to relative paths
  
  configFile_ = "/home/tg/catkin_ws/src/robot_fpv/models/yolov3/yolov3.cfg";
  weightFile_ = "/home/tg/catkin_ws/src/robot_fpv/models/yolov3/yolov3.weights";
  const std::string classFile = "/home/tg/catkin_ws/src/robot_fpv/models/yolov3/coco.names";
  std::ifstream ifs(classFile.c_str());
  std::string line;
  while (getline(ifs, line))
  {
    classes_.push_back(line);
  }
  
  //ROS_DEBUG_STREAM("OpenCV version: " << CV_VERSION);

  net_ = cv::dnn::readNetFromDarknet(configFile_, weightFile_);
	
  setNetBackend();

  detectedObjects_.reserve(MAX_DETECTED_OBJECTS);
}

ObjectDetectorYOLO::~ObjectDetectorYOLO() 
{

}

objVect ObjectDetectorYOLO::detect(cv::Mat& frame)
{
  if(frame.empty())
  {
    throw std::runtime_error("Frame is empty");
  }

  cv::Mat blob;

  cv::dnn::blobFromImage(frame, blob, inScaleFactor_, 
                          cvSize(inWidth_, inHeight_), Scalar(0,0,0), true, false);
        
        //Sets the input to the network
        net_.setInput(blob);
        
        // Runs the forward pass to get output of the output layers
        std::vector<Mat> outs;
        net_.forward(outs, getOutputsNames());
        
        // Remove the bounding boxes with low confidence
        postprocess(frame, outs);
        
        // Put efficiency information. The function getPerfProfile returns the overall time for inference(t) and the timings for each of the layers(in layersTimes)
        vector<double> layersTimes;
        double freq = getTickFrequency() / 1000;
        double t = net_.getPerfProfile(layersTimes) / freq;
        string label = format("Inference time for a frame : %.2f ms", t);
        putText(frame, label, Point(0, 15), FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0, 0, 255));
        
      // if (y1 < 0.1 * frame.cols)
      // {
      //   y1 += 50;
      // }

      //cv::putText(frame, label, cv::Point(x1 + 20, y1 - 10), cv::FONT_HERSHEY_SIMPLEX, 0.7, cv::Scalar(0,0,255), 2, cv::LINE_AA);
      //detectedObjects_.push_back(std::pair<const char*, cv::Rect2i>(classes_[classId].c_str(), detectedObjRect));

  return detectedObjects_;
}

} // namespace imgProc