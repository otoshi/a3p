//
//  Based on learnopencv object detector
//

#pragma once

#include "ObjectDetector.h"

namespace imgProc
{
class ObjectDetectorTF : public ObjectDetector
{

public:

  ObjectDetectorTF();
  virtual ~ObjectDetectorTF();

  objVect detect(cv::Mat& frame);

private:

};

} // namespace imgProc