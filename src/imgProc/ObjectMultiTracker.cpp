#include "ObjectMultiTracker.h"

#define OFFSET_ERROR_V 20
#define OFFSET_ERROR_H 20

namespace imgProc
{

inline cv::Point2d rectCenterPoint(cv::Rect2d rect)
{
  return cv::Point2d(rect.x+rect.width/2, rect.y+rect.height/2);
}

bool ObjectMultiTracker::add(cv::Ptr<cv::Tracker> newTracker, cv::InputArray image, 
                                const cv::Rect2d& boundingBox, int objId, const char* objName )
{
  std::vector<int> similarObjects;
  for (int i=0; i<trackerLabels_.size(); ++i)
  {
    if (trackerLabels_[i].second == objName)
    {
      similarObjects.push_back(i);
      ROS_DEBUG_STREAM( "Already see " << objName << " with index " << i );
    }
  }

  if (similarObjects.size() > 0)
  {
    cv::Point2d newObjCenter = rectCenterPoint(boundingBox);
    //for every object with matching label check for it's location
    for (int j = 0; j<similarObjects.size(); ++j)
    {
      cv::Point2d existingObjCenter = rectCenterPoint(objects[similarObjects[j]]);
      if (abs(newObjCenter.x - existingObjCenter.x) < OFFSET_ERROR_H &&
          abs(newObjCenter.y - existingObjCenter.y) < OFFSET_ERROR_V)
      {
        ROS_DEBUG_STREAM( "New object is considered existing " 
                  << trackerLabels_[similarObjects[j]].second
                  << " with its center at coordinates " 
                  << existingObjCenter );
      }
      return false;
    }
  }
  
  ROS_DEBUG_STREAM( "Adding new object: " << objName 
                  << " at position " << rectCenterPoint(boundingBox) ); 

  // add the tracker algorithm to the trackers list
  trackerList.push_back(newTracker);

  // add the ROI to the bounding box list
  objects.push_back(boundingBox);

  // add label to the new tracker
  trackerLabels_.push_back(std::pair(objId, objName));

  // initialize the created tracker
  return trackerList.back()->init(image, boundingBox);
}

// add a set of objects to be tracked
bool ObjectMultiTracker::add(std::vector<cv::Ptr<cv::Tracker> > newTrackers, cv::InputArray image, 
                                std::vector<cv::Rect2d> boundingBox, int objId, const char* objName)
{
  // status of the tracker addition
  bool stat = false;

  // add tracker for all input objects
  for(unsigned i = 0; i < boundingBox.size();i++)
  {
    stat = add(newTrackers[i],image,boundingBox[i], objId, objName);
    
    if(!stat)
    {
      break;
    }
  }

  // return the status
  return stat;
};

std::optional<std::vector<int>> ObjectMultiTracker::updateWithPresenceCheck(cv::InputArray image)
{
  std::vector<int> lostObjectIDs;
  for(int i = 0; i < trackerList.size(); i++)
  {
    bool status = trackerList[i]->update(image, objects[i]);
    
    if (status == false)
    {
      int id = trackerLabels_[i].first;
      const char* label = trackerLabels_[i].second;
      ROS_DEBUG_STREAM( "Lost track of " << label 
                          << " with id " << id );
      if (trackedObjectIndex_ != nullptr && id == *trackedObjectIndex_)
      {
        ROS_WARN_STREAM("Lost track of key tracked object: " << label 
                          << "! Select object to follow once again.");
        *trackedObjectIndex_ = -1;
      }
      lostObjectIDs.push_back(trackerLabels_[i].first);
      trackerList.erase(trackerList.begin()+i);
      objects.erase(objects.begin()+i);
      trackerLabels_.erase(trackerLabels_.begin()+i);
    }
  }

  if (lostObjectIDs.size() > 0)
  {
    return lostObjectIDs;
  }

  return std::nullopt;
}

} // namespace imgProc