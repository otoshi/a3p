#include "LaserDetector.h"

#undef DEBUG

namespace imgProc
{

constexpr int ROI_X_OFFSET = 200;
constexpr int ROI_Y_OFFSET = 150;
constexpr int ROI_X_SIZE = 200;
constexpr int ROI_Y_SIZE = 150; 

LaserDetector::LaserDetector(const char* imageGetterTopic)
{
  ic_ = std::make_unique<ImageConverter>(imageGetterTopic);
}

LaserDetector::~LaserDetector()
{
}

bool LaserDetector::getDistanceCb(a3p::DistanceMeas::Request  &req,
                                  a3p::DistanceMeas::Response &res)
{
  ros::spinOnce();
  cv::Mat inputImg = ic_->getImg();
  ROS_DEBUG_STREAM("Measuring distance for frame " << ic_->getFrameIndex());
  
  std::pair<double, double> result = computeLaserCoords(inputImg);
  res.distPx = result.first;
  res.angleDeg = result.second;
  
  return true;
}

} // namespace imgProc