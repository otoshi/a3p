#include "LaserDetectorBase.h"

#undef DEBUG

namespace imgProc
{

constexpr int ROI_X_OFFSET = 200;
constexpr int ROI_Y_OFFSET = 150;
constexpr int ROI_X_SIZE = 200;
constexpr int ROI_Y_SIZE = 150; 

LaserDetectorBase::LaserDetectorBase()
{
}

LaserDetectorBase::~LaserDetectorBase()
{
}


std::pair<double, double> LaserDetectorBase::computeLaserCoords(cv::Mat& img)
{
  if (img.empty())
  {
    std::cout << "Cannot analyze empty image!\n";
    return {-1, -1};
  }
  cv::Rect2i rec(ROI_X_OFFSET, ROI_Y_OFFSET, ROI_X_SIZE, ROI_Y_SIZE);

  cv::Mat crop = img(rec);

  cv::cvtColor(crop, crop, cv::COLOR_BGR2GRAY);

  std::vector<cv::Vec3f> circles;

  cv::HoughCircles(crop, circles, cv::HOUGH_GRADIENT,
                   1,  // dp
                   30, // minDist
                   45, // high Canny 
                   2,  // detector sensitivity
                   0,  // min radi
                   5); // max radi

  std::sort(circles.begin(), circles.end(), [](cv::Vec3f a, cv::Vec3f b){ return a[1] > b[1];});
  
  std::vector<cv::Vec3f> redCircles = filterCirclesByColor(img, circles);

  #ifdef DEBUG
  cv::rectangle(img, rec, cv::Scalar(0, 0, 255), 1, cv::LineTypes::LINE_AA);
  cv::imwrite("laser_detector_debug.jpg", img);
  #endif

  if (redCircles.size() != 2)
  {
    std::cout << "Laser ref points not detected! " << redCircles.size() << " points detected.\n";
    return {-1, -1};
  }

  cv::Point2i centerPoint(redCircles[0][0], redCircles[0][1]);
  cv::Point2i measPoint(redCircles[1][0], redCircles[1][1]);

  std::pair<double, double> res;

  res.first = pointPointDistance(centerPoint, measPoint);
  res.second = pointPointAngle(centerPoint, measPoint);

  #ifdef DEBUG
  std::cout << "Distance: " << res.first << " px. Angle: " << res.second << " px." << "\n";
  #endif

  return res;
}

bool LaserDetectorBase::pixInsideCircle(int x, int y, int r)
{
  return x >= -r && x < r && y >= -r && y < r;
}

bool LaserDetectorBase::circleMoreRedThanBg(const cv::Vec3i& circlePxColorSum, const cv::Vec3i& bgPxColorSum, 
                                        int circlePxCount, int bgPxCount)
{
  if (circlePxCount == 0 || bgPxCount == 0)
  {
    return false;
  }

  double circleRedAvg = (circlePxColorSum[2])/circlePxCount;
  double bgRedAvg = (bgPxColorSum[2])/bgPxCount;

  #ifdef DEBUG
  std::cout << "Avg red chn value inside circle: " << circleRedAvg << 
                   " Avg red chn value of the background: " << bgRedAvg << "\n";
  #endif

  return circleRedAvg > bgRedAvg;
}

bool LaserDetectorBase::circleIsRedDominated(const cv::Vec3i& circlePxColorSum)
{
  #ifdef DEBUG
  std::cout << "Red px sum inside circle: " << (int)circlePxColorSum[2] << 
                   " / blue green avg sum inside circle: " << 
                   (circlePxColorSum[0] + circlePxColorSum[1]) / 2 << "\n";
  #endif

  return circlePxColorSum[2] > (circlePxColorSum[0] + circlePxColorSum[1]) / 2;
}

std::vector<cv::Vec3f> LaserDetectorBase::filterCirclesByColor(cv::Mat& img, std::vector<cv::Vec3f>& circles)
{
  std::vector<cv::Vec3f> redCircles;

  for (auto &circle : circles) 
  {
    cv::Vec3i circlePxColorSum = {0, 0, 0};
    cv::Vec3i bgPxColorSum = {0, 0, 0};

    int circlePxCount = 0, bgPxCount = 0;
    int radius = circle[2];

    if (radius == 1)
    {
      radius++;
    }

    for (int i = -(3 * radius); i < (3 * radius); i++)
    {
      for (int j = -(3 * radius); j < (3 * radius); j++)
      {
        int x = circle[0] + ROI_X_OFFSET + i;
        int y = circle[1] + ROI_Y_OFFSET + j;

        if (x < 0 || x > img.cols || 
            y < 0 || y > img.rows)
        {
          continue; 
        }
        
        cv::Vec3b pix = img.at<cv::Vec3b>(y, x);
        
        if (pixInsideCircle(i, j, radius))
        {
          circlePxColorSum += pix;
          circlePxCount++;
        }
        else
        {
          bgPxColorSum += pix;
          bgPxCount++;
        }
      }
    }

    #ifdef DEBUG
    cv::circle(img, cv::Point2d(circle[0] + ROI_X_OFFSET, 
               circle[1] + ROI_Y_OFFSET), radius, cv::Scalar(0, 255, 0), 1);
    std::cout << "Point coords:" << " x: " << circle[0] << "\ty: " << circle[1] << "\tr: " << radius << "\n";
    #endif

    if (circleMoreRedThanBg(circlePxColorSum, bgPxColorSum, circlePxCount, bgPxCount) && 
        circleIsRedDominated(circlePxColorSum))
    {
      
      #ifdef DEBUG
      cv::circle(img, cv::Point2d(circle[0] + ROI_X_OFFSET, 
                 circle[1] + ROI_Y_OFFSET), radius, cv::Scalar(0, 0, 255), 1);
      #endif

      redCircles.push_back(circle);
    }
  }
  
  return redCircles;
}

double LaserDetectorBase::pointPointDistance(const cv::Point2i& a, const cv::Point2i& b)
{
  return sqrt(pow(a.x - b.x, 2) + pow(a.y - b.y, 2));
}

double LaserDetectorBase::pointPointAngle(const cv::Point2i& a, const cv::Point2i& b)
{
  double dx = b.x - a.x;
  double dy = a.y - b.y;
  return atan2(dy, dx) * 360 / (2 * M_PI);
}

} // namespace imgProc