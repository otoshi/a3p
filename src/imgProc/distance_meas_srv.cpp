#include "LaserDetector.h"

int main(int argc, char **argv)
{
  ros::init(argc, argv, "distance_meas_server");
  ros::NodeHandle n;
  
  imgProc::LaserDetector detector("/raspicam_node_turret/image");

  ros::ServiceServer srv = n.advertiseService("distance_meas", &imgProc::LaserDetector::getDistanceCb, &detector);

  ROS_INFO("Started distance meas server.");

  ros::spin();

  return 0;
}