//
// Simple Hough circles detector
//

#pragma once

#include "ObjectDetector.h"

namespace imgProc
{

class ObjectDetectorHoughC : public ObjectDetector
{

public:

  ObjectDetectorHoughC();
  virtual ~ObjectDetectorHoughC();

  objVect detect(cv::Mat& frame);

private:

};

} // namespace imgProc