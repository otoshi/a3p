#pragma once

#include <memory.h>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>

namespace imgProc
{

class LaserDetectorBase
{

public:
  LaserDetectorBase();
  ~LaserDetectorBase();

protected:

  std::pair<double, double> computeLaserCoords(cv::Mat& img);
  std::vector<cv::Vec3f> filterCirclesByColor(cv::Mat& img, std::vector<cv::Vec3f>& circles);
  
  double pointPointDistance(const cv::Point2i& a, const cv::Point2i& b);
  double pointPointAngle(const cv::Point2i& a, const cv::Point2i& b);
  bool   pixInsideCircle(int x, int y, int r);
  bool   circleMoreRedThanBg(const cv::Vec3i& circlePxColorSum, const cv::Vec3i& bgPxColorSum, 
                             int circlePxCount, int bgPxCount);
  bool   circleIsRedDominated(const cv::Vec3i& circlePxColorSum);

};

}