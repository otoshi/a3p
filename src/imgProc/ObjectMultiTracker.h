#pragma once

#include <ros/ros.h>
#include <opencv2/tracking.hpp>
#include <optional>

namespace imgProc
{

class ObjectMultiTracker : public cv::MultiTracker
{
  public:

  static cv::Ptr<ObjectMultiTracker> create()
  {
    return cv::makePtr<ObjectMultiTracker>();
  }

  //rename
  bool add(cv::Ptr<cv::Tracker> newTracker, cv::InputArray image,
              const cv::Rect2d& boundingBox, int objId, const char* objName);
                                  
  bool add(std::vector<cv::Ptr<cv::Tracker> > newTrackers, cv::InputArray image,
              std::vector<cv::Rect2d> boundingBox, int objId, const char* objName);

  std::optional<std::vector<int>> updateWithPresenceCheck(cv::InputArray image); 

  std::vector<std::pair<int, const char*>> getLabels()
  {
    return trackerLabels_;
  }

  void setTrackedObject(int* objectIndex)
  {
    trackedObjectIndex_ = objectIndex;
  }

  protected:

  std::vector<std::pair<int, const char*>> trackerLabels_; 
  int* trackedObjectIndex_ = nullptr;

};

} // namespace imgProc