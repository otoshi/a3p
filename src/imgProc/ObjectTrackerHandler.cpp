#include "ObjectTrackerHandler.h"

namespace imgProc
{

ObjectTrackerHandler::ObjectTrackerHandler()
{
  tracker_ = ObjectMultiTracker::create();
}

void ObjectTrackerHandler::setTrackedObject(int* trackedObjectIndex)
{
  tracker_->setTrackedObject(trackedObjectIndex);
}

ObjectTrackerHandler::ObjectTrackerHandler(TrackerType type)
{
  std::string_view strType;
  switch (type)
  {
  case TrackerType::BOOSTING:
    strType = "BOOSTING";
    break;
  case TrackerType::CSRT:
    strType = "CSRT";
    break;
  case TrackerType::GOTURN:
    strType = "GOTURN";
    break;
  case TrackerType::KCF:
    strType = "KCF";
    break;
  case TrackerType::MEDIANFLOW:
    strType = "MEDIANFLOW";
    break;
  case TrackerType::MIL:
    strType = "MIL";
    break;
  case TrackerType::MOSSE:
    strType = "MOSSE";
    break;
  case TrackerType::TLD:
    strType = "TLD";
    break;
  default:
    throw cv::Exception(0, "Unknown tracker type", "ObjectTracker()", __FILE__, __LINE__);
    break;
  }

  if (setTrackerType(strType) < 0)
  {
    throw cv::Exception(0, "Unknown tracker type", "ObjectTracker()", __FILE__, __LINE__);
  }
  tracker_ = ObjectMultiTracker::create();
} 

int ObjectTrackerHandler::setTrackerType(std::string_view type)
{
  if(std::find(trackerTypes_.begin(), trackerTypes_.end(), type) != trackerTypes_.end())
  {
    trackerType_ = type;

    return 1;
  }
  else 
  {
    ROS_WARN_STREAM("Incorrect tracker name");
    ROS_WARN_STREAM("Available trackers are: ");
    for (auto it = trackerTypes_.begin() ; it != trackerTypes_.end(); ++it)
    {
      ROS_WARN_STREAM(" " << *it);
    }
  
    return -1;
  }
}

void ObjectTrackerHandler::refresh(cv::Mat& frame, objVect detectedObjects)
{
  if (!frame.empty() && detectedObjects.size() > 0)
  {
    int i = 0;
    for (auto& object : detectedObjects)
    {
      ROS_DEBUG_STREAM("Adding object to track");
      ROS_DEBUG_STREAM(trackerType_);
      tracker_->add(createTrackerByName(trackerType_), frame, object.second, i, object.first);
      i++;
    }
  }
}

cv::Ptr<cv::Tracker> ObjectTrackerHandler::createTrackerByName(std::string_view trackerType)
{
  cv::Ptr<cv::Tracker> tracker;
  if (trackerType ==  trackerTypes_[0])
    tracker = cv::TrackerBoosting::create();
  else if (trackerType == trackerTypes_[1])
    tracker = cv::TrackerMIL::create();
  else if (trackerType == trackerTypes_[2])
    tracker = cv::TrackerKCF::create();
  else if (trackerType == trackerTypes_[3])
    tracker = cv::TrackerTLD::create();
  else if (trackerType == trackerTypes_[4])
    tracker = cv::TrackerMedianFlow::create();
  else if (trackerType == trackerTypes_[5])
    tracker = cv::TrackerGOTURN::create();
  else if (trackerType == trackerTypes_[6])
    tracker = cv::TrackerMOSSE::create();
  else if (trackerType == trackerTypes_[7])
    tracker = cv::TrackerCSRT::create();

  return tracker;
}

bool ObjectTrackerHandler::update(cv::Mat& frame, objVect& objects)
{
  bool status = true;
  auto objectLost = tracker_->updateWithPresenceCheck(frame);

  if (!objectLost && tracker_->getObjects().size() == objects.size())
  {
    for(unsigned i=0; i<tracker_->getObjects().size(); i++)
    {
      objects[i].first = tracker_->getLabels()[i].second;
      objects[i].second = tracker_->getObjects()[i];
    }
  }
  else
  {
    status = false;
    tracker_->clear();
    ROS_DEBUG_STREAM("Lost some objects");
  }
  
  return status;
}

} // namespace imgProc