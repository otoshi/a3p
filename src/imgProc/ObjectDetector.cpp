#include "ObjectDetector.h"

namespace imgProc
{

ObjectDetector::ObjectDetector() 
{}

ObjectDetector::~ObjectDetector() 
{}

void ObjectDetector::setNetBackend() 
{
  #if (CV_VERSION_MAJOR > 3 && USE_CUDA)
  //ROS_DEBUG_STREAM("Using CUDA acceleration.");
  net_.setPreferableBackend(cv::dnn::DNN_BACKEND_CUDA);
  net_.setPreferableTarget(cv::dnn::DNN_TARGET_CUDA);
  #else
  //ROS_DEBUG_STREAM("Using CPU.");
  net_.setPreferableBackend(cv::dnn::DNN_BACKEND_OPENCV);
  net_.setPreferableTarget(cv::dnn::DNN_TARGET_CPU);
  #endif
}

} // namespace imgProc