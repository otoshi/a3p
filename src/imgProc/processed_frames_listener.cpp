#include <ros/ros.h>
#include "ImageConverter.h"
#include "ObjectDetectorTF.h"


int main (int argc, char** argv)
{
  ros::init(argc, argv, "detector_node");

  imgProc::ImageConverter ic("raw_frames");
  imgProc::ObjectDetectorTF detector;

  int fps = 32; //default value

  if (ros::param::has("framerate"))
  {
    ros::param::get("framerate", fps);
    ROS_INFO_ONCE("Ros listener started with set framerate: %i", fps);
  }
  else
  {
    ROS_INFO_ONCE("Ros listener started with default framerate: %i", fps);
  }

  ros::Rate frame_rate(fps);
  while (ros::ok())
  {
    cv::Mat frame = ic.getImg();
    if (!frame.empty())
    {
      try
      {
        detector.detect(frame);
      }
      catch(cv::Exception ex)
      {
        ROS_INFO_ONCE(ex.what());
      }
      cv::waitKey(1);
    }
    ros::spinOnce();
    frame_rate.sleep();
  }
  
  return 0;
}


