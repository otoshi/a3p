#pragma once

#include "LaserDetectorBase.h"
#include "a3p/DistanceMeas.h"
#include "ImageConverter.h"

namespace imgProc
{
class LaserDetector : public LaserDetectorBase
{
public:

  explicit LaserDetector(const char* imageGetterTopic);
  ~LaserDetector();

  bool getDistanceCb(a3p::DistanceMeas::Request &req, a3p::DistanceMeas::Response &res);

protected:

  std::unique_ptr<imgProc::ImageConverter> ic_ = nullptr;
};

}