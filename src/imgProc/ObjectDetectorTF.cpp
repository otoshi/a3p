#include "ObjectDetectorTF.h"

#define TF_CONFIG PROJECT_HOME_DIR "/models/tensorFlow/ssd_mobilenet_v2_coco_2018_03_29.pbtxt"
#define TF_WEIGHT PROJECT_HOME_DIR "/models/tensorFlow/ssd_mobilenet_v2_frozen_inference_graph.pb"
#define TF_LABELS PROJECT_HOME_DIR "/models/tensorFlow/coco_class_labels.txt"

namespace imgProc
{

ObjectDetectorTF::ObjectDetectorTF() 
{
  configFile_ = TF_CONFIG;
  weightFile_ = TF_WEIGHT;
  std::ifstream labelFile(TF_LABELS);
  std::string line;
  while (getline(labelFile, line))
  {
    classes_.push_back(line);
  }

  labelFile.close();
  
  // ROS_INFO_STREAM("OpenCV version: " << CV_VERSION);
  // ROS_INFO_STREAM("Loading model from: " << TF_WEIGHT);
  
  net_ = cv::dnn::readNetFromTensorflow(weightFile_, configFile_);

  setNetBackend();

  detectedObjects_.reserve(MAX_DETECTED_OBJECTS);
}

ObjectDetectorTF::~ObjectDetectorTF() 
{

}

objVect ObjectDetectorTF::detect(cv::Mat& frame)
{
  if(frame.empty())
  {
    throw std::runtime_error("Frame is empty");
  }
  cv::Mat inputBlob = cv::dnn::blobFromImage(frame, inScaleFactor_, cv::Size(inWidth_, inHeight_), meanVal, true, false);
  net_.setInput(inputBlob);
  cv::Mat detection = net_.forward("detection_out");
  cv::Mat detectionMat(detection.size[2], detection.size[3], CV_32F, detection.ptr<float>());

  detectedObjects_.clear();

  for(int i = 0; i < detectionMat.rows; i++)
  {
    float confidence = detectionMat.at<float>(i, 2);
    int classId = detectionMat.at<float>(i, 1);

    if(confidence > confidenceThreshold_)
    {
      int x1 = static_cast<int>(detectionMat.at<float>(i, 3) * frame.cols);
      int y1 = static_cast<int>(detectionMat.at<float>(i, 4) * frame.rows);
      int x2 = static_cast<int>(detectionMat.at<float>(i, 5) * frame.cols);
      int y2 = static_cast<int>(detectionMat.at<float>(i, 6) * frame.rows);

      //std::string label = cv::format("%s %.3f", classes_[classId].c_str(), confidence);
      cv::Rect2i detectedObjRect(cv::Point2i(x1, y1), cv::Point2i(x2, y2));

      detectedObjects_.push_back(std::pair<const char*, cv::Rect2i>(classes_[classId].c_str(), detectedObjRect));
    }
  }

  return detectedObjects_;
}

} // namespace imgProc