#include "ImgProcHandler.h"
#include "ObjectDetectorTF.h"
#include "ObjectDetectorYOLO.h"
#include "ObjectDetectorHoughC.h"
#include "util/SpeedTest.h"

#define DETECTION_INTERVAL 10

#undef TEST
#undef DETECT_SIMPLE_CIRCLE

namespace imgProc
{

ImgProcHandler::ImgProcHandler()
{
  #ifdef TEST
  ic_ = std::make_unique<ImageConverter>("raw_frames");
  #else
  ic_ = std::make_unique<ImageConverter>("/raspicam_node_turret/image");
  #endif

  #ifdef DETECT_SIMPLE_CIRCLE
  detector_ = std::make_unique<ObjectDetectorHoughC>();
  #else
  detector_ = std::make_unique<ObjectDetectorTF>();
  #endif

  tracker_  = std::make_unique<ObjectTrackerHandler>(ObjectTrackerHandler::TrackerType::MEDIANFLOW);
}

cv::Mat& ImgProcHandler::getRawImg()
{
  return ic_->getImg();
}

int ImgProcHandler::getFrameIndex()
{
  return ic_->getFrameIndex();
}

void ImgProcHandler::setTrackedObject(int* objectIndex)
{
  tracker_->setTrackedObject(objectIndex);
}

objVect ImgProcHandler::detectObjects(cv::Mat& frame)
{
  static bool lostTrackOfSth = true;
  if (ic_->getFrameIndex() % DETECTION_INTERVAL == 0 || lostTrackOfSth)
  {
    try
    {
      auto start = SpeedTest::msMeasStart();
      detected_ = detector_ -> detect(frame);
      auto detectionTime = SpeedTest::msMeasStop(start);
      ROS_DEBUG_THROTTLE(0.5, "Detected %i objects:", detected_.size());
      ROS_DEBUG_STREAM("Detection took " << detectionTime << " ms");
      for (const auto& object : detected_)
      {
        ROS_DEBUG("%s at (%i,%i)", object.first, 
                (object.second.x + object.second.width ) / 2,
                (object.second.y + object.second.height) / 2);
      }
      lostTrackOfSth = false;
    }
    catch(cv::Exception& ex)
    {
      ROS_WARN_THROTTLE(0.5, ex.what());
    }
    tracker_->refresh(frame, detected_);
    framesAfterLastDetection++;
  }
  else
  {
    if (tracker_->update(frame, detected_) == false)
    {
      lostTrackOfSth = true;
    }
  }

  return detected_;
}

} // namespace imgProc