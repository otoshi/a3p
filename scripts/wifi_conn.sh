#!/bin/bash
interface=$(ifconfig | grep "wl*"| awk '{print substr($1, 1, length($1)-1)}')
echo "$interface"
secs=1
until [ $secs -eq 60 ]; do
    nmcli d connect "$interface"
    status=$?
    echo "$status after $secs attempt."
    if [ $status -eq '0' ]; then
        break
    fi
    sleep 1
    let secs+=1
done

echo "Command finished with status $status after $secs attempts."